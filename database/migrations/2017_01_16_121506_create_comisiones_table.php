<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComisionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         
         Schema::create('comisiones', function (Blueprint $table) {
                    
                    $table->increments('id');
                    $table->string('descripcion');
                    
                    $table->date('fecha_inicio');
                    $table->date('fecha_fin');

                    $table->decimal('duracion');

                    $table->integer('materia_id');
                   

                    $table->foreign('materia_id')->references('id')->on('materias');

            
                    $table->timestamps();
                }); 

     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comisiones');
    }
}
