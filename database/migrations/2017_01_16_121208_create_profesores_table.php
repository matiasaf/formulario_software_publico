<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfesoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('profesores', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('nombre');
                    $table->string('apellido');
                    
                    $table->string('dni');
                    $table->integer('edad');
                    
                    $table->date('fecha_nacimiento');
                    $table->string('domicilio');
                    $table->string('telefono_fijo');
                    $table->string('telefono_celular');


                    $table->foreign('localidad_id')->references('id')->on('localidades');

            
                    $table->timestamps();
                });  

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profesores');
    }
}
