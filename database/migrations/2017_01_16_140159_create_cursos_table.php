<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('cursos', function (Blueprint $table) {
                    
                    $table->increments('id');
                    $table->string('nombre');
                    $table->integer('competencia_transversal');
                    
                    $table->integer('profesor_id');

                    $table->integer('nomenclatura_moodle');
                    
                    $table->date('año');
                    $table->date('fecha_fin');

                    $table->decimal('duracion');

                    $table->integer('materia_id');
                   

                    $table->foreign('materia_id')->references('id')->on('materias');
                    $table->foreign('profesor_id')->references('id')->on('profesores');

            
                    $table->timestamps();
                }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
