<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMateriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materias', function (Blueprint $table) {
                   
            $table->increments('id');
            $table->string('nombre');

            $table->string('comp_transversal');
            $table->string('url_pdf_contenidos_minimos');

            $table->integer('categoria_id');
            $table->integer('estado');
            

            $table->foreign('categoria_id')->references('id')->on('cap_categoria');

        
        }); 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('materias');
    }
}
