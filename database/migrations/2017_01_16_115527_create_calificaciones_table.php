<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalificacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cap_calificacion', function (Blueprint $table) {
                   
            $table->increments('id');

            $table->string('nota');
            

            $table->integer('alumno_id');
            $table->integer('materia_id');

            $table->date('fecha');
            
            $table->integer('audit_usuario_id');
            

            $table->foreign('alumno_id')->references('id')->on('alumnos');
            $table->foreign('materia_id')->references('id')->on('materias');

                  
            $table->timestamps();
        
        }); 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cap_calificacion');
    }
}
