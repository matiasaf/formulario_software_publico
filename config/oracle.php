<?php

return [
    'oracle' => [
        'driver'   => 'oracle',
        'tns'      => env('DB_TNS', ''),
        'host'     => env('DB_HOST', '10.10.2.17'),
        'port'     => env('DB_PORT', '1526'),
        'database' => env('DB_DATABASE', 'DESA'),
        'username' => env('DB_USERNAME', 'RPI'),
        'password' => env('DB_PASSWORD', '2015rPi2015'),
        'charset'  => env('DB_CHARSET', 'AL32UTF8'),
        'prefix'   => env('DB_PREFIX', ''),
    ],
];
