$(document).ready(function () {

    //MUESTRA INICIO DE SESION MODAL
    $(window).load(function () {
        $('#myModal').modal('show');
    });
    //NO PERMITE CERRARLO
    $('#myModal').modal({backdrop: 'static', keyboard: false});

    $('#filtro').change(function () {
        $("input[value=Buscar]").prop('disabled', true);
        if ($('#filtro option:selected').text() === 'Año') {
            $("input[name='tomo']").val('');
            $('#año').css('display', 'inline');
            $("input[name='tomo']").parent().removeClass('has-success').removeClass('has-error');
            $("input[name='tomo']").next().removeClass('glyphicon glyphicon-ok').removeClass('glyphicon glyphicon-remove');
            $("input[name='tomo']").next().next().html('');
            $('#tomo').css('display', 'none');
        } else {
            $("input[name='anio']").val('');
            $('#tomo').css('display', 'inline');
            $("input[name='anio']").parent().removeClass('has-success').removeClass('has-error');
            $("input[name='anio']").next().removeClass('glyphicon glyphicon-ok').removeClass('glyphicon glyphicon-remove');
            $("input[name='anio']").next().next().html('');
            $('#año').css('display', 'none');
        }

    });

    //VALIDACIONES CAMPOS DE BUSQUEDAD Y AGREGADO
    //FORM BUSCAR
    $("#frm-digB input[value=Buscar]").prop('disabled', true);

    $("#frm-digB input[name='tomo']").keyup(function () {
        numeros2(this);
    });

    $("#frm-digB input[name='anio']").keyup(function () {
        año(this);
    });

    $("#frm-digB input[name='folio']").keyup(function () {
        numeros2(this);
    });

    $("#frm-digB input[name='tomo'], [name='anio'], [name='folio']").keyup(function () {
        if (($("input[name='tomo']").parent().hasClass('form-group has-feedback has-success') ||
                $("input[name='anio']").parent().hasClass('form-group has-feedback has-success')) &&
                $("input[name='folio']").parent().hasClass('form-group has-feedback has-success')) {

            $("input[value=Buscar]").prop('disabled', false);
        } else {
            $("input[value=Buscar]").prop('disabled', true);
        }
    });


    //FORM AGREGAR

    $("input[name=guardar]").prop('disabled', true);
    $("input[name=mas]").prop('disabled', true);
    $("#frm-digA input[name='anio']").prop('disabled', true);
    $("#frm-digA input[name='cant']").prop('disabled', true);
    $("input[type=file]").prop('disabled', true);
    $("input[type=file]").parent().css('background-color', '#eee');
    $("input[type=file]").css('cursor', 'not-allowed');

    //CARGA DE PDF
    $("input[type=file]").change(function () {
        var cant = $(this)[0].files.length;
        var mb = 0;
        var ext = new Array();
        var tam = new Array();
        for (var i = 0; i < cant; i++) {
            mb = this.files[i].size + mb;
            if (this.files[i].name.substring(this.files[i].name.length - 3).toLowerCase() !== 'pdf') {
                ext[i] = this.files[i].name.substring(this.files[i].name.length - 3);
            }
            if (this.files[i].size > 8388608) {
                tam[i] = this.files[i].name.substring(0, this.files[i].name.length - 4)+'<br />';
            }
        }
        $("input[name=guardar]").prop('disabled', true);
        $("input[name=mas]").prop('disabled', true);
        if (cant > 100) {
            $(this).parent().parent().removeClass('has-success').addClass('has-error');
            $(this).parent().next().removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-remove');
            $(this).parent().next().next().html('Puedes subir 100 como máximo.');
            $(this).val('');
        } else if (mb > 41943040) {
            $(this).parent().parent().removeClass('has-success').addClass('has-error');
            $(this).parent().next().removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-remove');
            $(this).parent().next().next().html('Puedes subir 40MB como máximo. (Seleccione menos archivos)');
            $(this).val('');
        } else if (ext.length > 0) {
            $(this).parent().parent().removeClass('has-success').addClass('has-error');
            $(this).parent().next().removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-remove');
            $(this).parent().next().next().html('Solo puedes subir archivos PDF.');
            $(this).val('');
        } else if (tam.length > 0) {
            $(this).parent().parent().removeClass('has-success').addClass('has-error');
            $(this).parent().next().removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-remove');
            $(this).parent().next().next().html('Se pueden subir archivos de 8mb como máximo <br />'+ tam.join(""));
            $(this).val('');
        } else {

            var folios = new Array();
            for (var i = 0; i < cant; i++) {
                var file = $(this)[0].files[i];
                if (file) {
                    folios[i] = file.name.substring(0, file.name.length - 4);
                }
            }
            $.ajax({
                url: 'index.php?c=Digitalizacion&a=CheckF',
                type: 'get',
                dataType: "json",
                data: {
                    f: folios,
                    t: $("#frm-digA input[name='tomo']").val(),
                },
                success: function (resultado) {
                    if (resultado.length == 0) {
                        $("input[type=file]").parent().parent().removeClass('has-error').addClass('has-success');
                        $("input[type=file]").parent().next().removeClass('glyphicon glyphicon-remove').addClass('glyphicon glyphicon-ok');
                        $("input[type=file]").parent().next().next().html('');
                        $("#frm-digA input[name='tomo']").prop('readonly', true);
                        $("#frm-digA input[name='anio']").prop('readonly', true);
                        $("#frm-digA input[name='cant']").prop('readonly', true);
                        $("#frm-digA select[name='ru']").attr('disabled', true);
                        if ($("input[name=estado]").val() === 'f') {
                            $("input[name=mas]").parent().css('display', '');
                            $("input[name=mas]").prop('disabled', false);
                            $("input[name=guardar]").parent().css('display', 'none');
                        } else {
                            $("input[name=guardar]").prop('disabled', false);
                        }
                    } else {
                        $("input[type=file]").parent().parent().removeClass('has-success').addClass('has-error');
                        $("input[type=file]").parent().next().removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-remove');
                        var existen = new Array();
                        for (var i = 0; i < resultado.length; i++) {
                            existen[i] = resultado[i] + '<br />';
                        }
                        $("input[type=file]").parent().next().next().html('Ya se encuentran cargados: ' + existen.join(""));
                        $("input[type=file]").val('');
                    }
                }
            });
        }
    });

    $("#frm-digA input[name='guardar']").click(function () {
        $("#frm-digA select[name='ru']").attr('disabled', false);
    })

    $("#frm-digA input[name='tomo']").keyup(function () {
        numeros(this);
    });

    $("#frm-digA input[name='cant']").keyup(function () {
        numeros2(this);
    });

    $("#frm-digA input[name='anio']").keyup(function () {
        año(this);
    });

    $("#frm-digA input[name='tomo'], [name='anio'], [name='cant']").keyup(function () {
        if ($("input[name='tomo']").parent().hasClass('form-group has-feedback has-success') &&
                $("input[name='anio']").parent().hasClass('form-group has-feedback has-success') &&
                $("input[name='cant']").parent().hasClass('form-group has-feedback has-success')) {

            $("input[type=file]").prop('disabled', false);
            $("input[type=file]").parent().css('background-color', '#fff');
            $("input[type=file]").css('cursor', 'pointer');
        } else {
            $("input[type=file]").prop('disabled', true);
            $("input[type=file]").parent().css('background-color', '#eee');
            $("input[type=file]").css('cursor', 'not-allowed');
        }
    });

    function checkT(v) {
        $.ajax({
            url: 'index.php?c=Digitalizacion&a=CheckT',
            type: 'get',
            dataType: "json",
            data: {
                v: $(v).val(),
            },
            success: function (resultado) {
                if (resultado[0] == 0) {
                    $(v).parent().removeClass('has-success').addClass('has-error');
                    $(v).next().removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-remove');
                    $(v).next().next().html('Ese tomo está completo.');
                    $("#frm-digA input[name='anio']").prop('disabled', true);
                } else if (resultado[0] > 0) {
                    $(v).parent().removeClass('has-error').addClass('has-success');
                    $(v).next().removeClass('glyphicon glyphicon-remove').addClass('glyphicon glyphicon-ok');
                    $(v).next().next().html('Faltan ' + resultado[0] + ' folios.');
                    $("#frm-digA input[name='anio']").prop('disabled', true);
                    $("#frm-digA input[name='cant']").prop('disabled', true);
                    $("#frm-digA select[name='ru']").prop('disabled', true);
                    $("#frm-digA input[name='anio']").val(resultado[1]);
                    $("#frm-digA input[name='cant']").val(resultado[2]);
                    $("#frm-digA input[name='estado']").val('f');
                    $("input[type=file]").prop('disabled', false);
                    $("input[type=file]").parent().css('background-color', '#fff');
                    $("input[type=file]").css('cursor', 'pointer');
                } else {
                    $(v).parent().removeClass('has-error').addClass('has-success');
                    $(v).next().removeClass('glyphicon glyphicon-remove').addClass('glyphicon glyphicon-ok');
                    $(v).next().next().html('Nuevo');
                    $("#frm-digA input[name='anio']").prop('disabled', false);
                    $("#frm-digA input[name='estado']").val('p');
                }
            }
        });
    }


    function numeros2(v) {
        var ex = /^[0-9]*$/;
        if (v.value.length === 0) {
            $(v).parent().removeClass('has-success').addClass('has-error');
            $(v).next().removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-remove');
            $(v).next().next().html('Ingrese un número.');
        } else {
            if (ex.test(v.value) === false) {
                $(v).parent().removeClass('has-success').addClass('has-error');
                $(v).next().removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-remove');
                $(v).next().next().html('Ingrese solo números.');
            } else {
                $(v).parent().removeClass('has-error').addClass('has-success');
                $(v).next().removeClass('glyphicon glyphicon-remove').addClass('glyphicon glyphicon-ok');
                $(v).next().next().html('');
            }
        }
    }

    function numeros(v) {
        var ex = /^[0-9]*$/;
        $("#frm-digA input[name='anio']").val('');
        $("#frm-digA input[name='cant']").val('');
        $("#frm-digA select[name='ru']").prop('disabled', false);
        if (v.value.length === 0) {
            $(v).parent().removeClass('has-success').addClass('has-error');
            $(v).next().removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-remove');
            $(v).next().next().html('Ingrese un número.');
            $("#frm-digA input[name='anio']").prop('disabled', true);
            $("#frm-digA input[name='anio']").parent().removeClass('has-success').removeClass('has-error');
            $("#frm-digA input[name='anio']").next().removeClass('glyphicon glyphicon-ok').removeClass('glyphicon glyphicon-remove');
            $("#frm-digA input[name='anio']").next().next().html('');
            $("#frm-digA input[name='cant']").prop('disabled', true);
            $("#frm-digA input[name='cant']").parent().removeClass('has-success').removeClass('has-error');
            $("#frm-digA input[name='cant']").next().removeClass('glyphicon glyphicon-ok').removeClass('glyphicon glyphicon-remove');
            $("#frm-digA input[name='cant']").next().next().html('');
        } else {
            if (ex.test(v.value) === false) {
                $(v).parent().removeClass('has-success').addClass('has-error');
                $(v).next().removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-remove');
                $(v).next().next().html('Ingrese solo números.');
                $("#frm-digA input[name='anio']").prop('disabled', true);
                $("#frm-digA input[name='anio']").parent().removeClass('has-success').removeClass('has-error');
                $("#frm-digA input[name='anio']").next().removeClass('glyphicon glyphicon-ok').removeClass('glyphicon glyphicon-remove');
                $("#frm-digA input[name='anio']").next().next().html('');
                $("#frm-digA input[name='cant']").prop('disabled', true);
                $("#frm-digA input[name='cant']").parent().removeClass('has-success').removeClass('has-error');
                $("#frm-digA input[name='cant']").next().removeClass('glyphicon glyphicon-ok').removeClass('glyphicon glyphicon-remove');
                $("#frm-digA input[name='cant']").next().next().html('');
            } else {
                checkT(v);
            }
        }
    }

    function año(v) {
        var ex = /^[0-9]*$/;
        $("#frm-digA input[name='cant']").prop('disabled', true);
        if (ex.test(v.value) === false) {
            v.value = v.value.substring(0, v.value.length - 1);
        }
        if (v.value.length !== 4) {
            $(v).parent().removeClass('has-success').addClass('has-error');
            $(v).next().removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-remove');
            $(v).next().next().html('Ingrese una año válido.');
        } else {
            ex = /^(188[8-9]|189[0-9]|19[0-9][0-9]|200\d|2015)$/;
            if (ex.test(v.value) === false) {
                v.value = v.value.substring(0, v.value.length - 1);
                $(v).parent().removeClass('has-success').addClass('has-error');
                $(v).next().removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-remove');
                $(v).next().next().html('Libros cargados desde 1888');
            } else {
                $(v).parent().removeClass('has-error').addClass('has-success');
                $(v).next().removeClass('glyphicon glyphicon-remove').addClass('glyphicon glyphicon-ok');
                $(v).next().next().html('');
                $("#frm-digA input[name='cant']").prop('disabled', false);
            }
        }

    }
})