
function validarFormModificacion(){


	global_error=true;


		if($('#decreto_numero').val() ==""){
			
			$('#lista_errores').append('<li id="li-decreto">Número de decreto no ingresado</li>');
			
			//pinto en rojo el label indicando donde se cometio el error
			$('#lbl-numero-decreto').css({'background-color':'#d9534f'});
			
			if( !$('#div_errores').is(":visible") ){

					$('#div_errores').show();
						
			}

			global_error=false;

		}

		
		if(($('#desc_fiscal').val() == 1 || $('#desc_fiscal').val() == 2 || $('#desc_fiscal').val() == 3) && ($('#pormil').val()=="") ){
			
			
			$('#lista_errores').append('<li id="li-decreto">Debe ingresar el valor del pormil a aplicar en el descuento fiscal</li>');
			
			//pinto en rojo el label indicando donde se cometio el error
			$('#lbl-pormil').css({'background-color':'#d9534f'});
			
			
			if( !$('#div_errores').is(":visible") ){

						$('#div_errores').show();
		
				}

			global_error=false;

		}


		if($('#decreto_numero').val() == "0" ){
			
			$('#lista_errores').append('<li id="li-decreto">Ingrese un número de decreto que sea distinto a 0 (Cero)</li>');
			
			//pinto en rojo el label indicando donde se cometio el error
			$('#lbl-numero-decreto').css({'background-color':'#d9534f'});

			if( !$('#div_errores').is(":visible") ){

						$('#div_errores').show();
		
			}

			global_error=false;

		}



	return global_error;

}

function limpiar_errores(){

	// console.log("el div de error esta visibile : "+$('#div_errores').is(":visible"));

	$('#div_errores').hide();

	//seteo en grises los labels
	$('#lbl-numero-decreto').css({'background-color':'#777'});
	$('#lbl-pormil').css({'background-color':'#777'});

	
	$("#lista_errores").empty();

}