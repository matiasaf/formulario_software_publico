$( document ).ready(function() {


	$('#btn-search-valor-fiscal').click( function(){

		$.ajax({
			url: '?c=ListadoActos&a=buscarListadoHistoricoAjax',
			type: 'POST',
			dataType: 'json',
			data: {
				codigo_descuento: $('#select_descuento_fiscal').val(),
			},
			success: function (resultado) {


				$("#tabla_historico_valores thead").remove();
				$("#tabla_historico_valores tbody").remove();


				var head_tabla ="<tr>"
				+		"<th style='text-align: center;'>Descripción</th>"
				+		"<th style='text-align: center;'>Fecha en Vigencia</th>"
				+		"<th style='text-align: center;'>Decreto Nº</th>"
				+		"<th style='text-align: center;'>Minimo</th>"
				+		"<th style='text-align: center;'>Máximo</th>"
				+		"<th style='text-align: center;'>Pormil (.%)</th>"
				+		"<th style='text-align: center;'>Vigente</th>"

				+"</tr>";


				$('#tabla_historico_valores').append(head_tabla);


				for (i=0; i<resultado.length; i++){


				var fecha= resultado[i].FEC_EN_VIGENCIA ? moment(resultado[i].FEC_EN_VIGENCIA).format("DD/MM/YYYY") : "";
				
				var nro_decreto= resultado[i].NRO_DECRETO ? resultado[i].NRO_DECRETO : "";
				var minimo= resultado[i].MNI ? resultado[i].MNI : "";
				var maximo= resultado[i].MMO ? resultado[i].MMO : "";
				var pormil= resultado[i].POR ? resultado[i].POR : "";
				var vigente= resultado[i].VIG ? resultado[i].VIG : "";

					var body_tabla="<tr>"
					+"<td style='vertical-align: middle; text-align: center;'>"
					+ castear_descripcion(resultado[i])
					+"</td>"
					+"<td style='vertical-align: middle; text-align: center;'>"
					+ fecha
					+"</td>"
					+"<td style='vertical-align: middle; text-align: center;'>"
					+ nro_decreto
					+"</td>"
					+"<td style='vertical-align: middle; text-align: center;'>"
					+ minimo
					+"</td>"
					+"<td style='vertical-align: middle; text-align: center;'>"
					+ maximo
					+"</td>"
					+"<td style='vertical-align: middle; text-align: center;'>"
					+ pormil
					+"</td>"
					+"<td style='vertical-align: middle; text-align: center;'>"
					+ vigente
					+"</td>"
					+"<tr>";


					$("#tabla_historico_valores tbody").append(body_tabla);


	         }//end for
	     	}//end success ajax

	      });//end peticion

	});

	$('#btn-generar-pdf-historico').click(function(){

	$('#codigo_descuento').val( $('#select_descuento_fiscal').val() );

	$('#formulario_generar_pdf_historico').submit();

	});



});