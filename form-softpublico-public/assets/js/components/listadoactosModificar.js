$(document).ready(function() {


	var url= location.href;

	var global_codigo_fiscal;
	var global_nueva_descripcion;


	//cuando carga la página, triggeo las siguientes funciones
	$(window).load(function () {

		verificar_hasta_entre_mayor();

	});


	$('#decreto_numero').change(function(){

		// comprobar_pertenece_numero_decreto_AJAX();
		limpiar_lista();
		pintar_select_desc_fiscal_segun_decreto();

	});


	$('#desc_fiscal').change(function(){



		verificar_hasta_entre_mayor();

		
		var codigo_valor_fiscal=$('#desc_fiscal').val();
		

		$.ajax({
			url: '?c=ListadoActos&a=buscarCamposValorFiscalAJAX',
			type: 'POST',
			dataType: 'json',
			data: {
				codigo_valor_fiscal: codigo_valor_fiscal,
			},
			success: function (resultado) {


				$('#importe_minimo').val(resultado.MNI);
				$('#importe_maximo').val(resultado.MMO);


				console.log(resultado.DES.substr(0,5));

				if(resultado.DES.substr(0,5)+" ..." =='hasta ...'){

					$('#val_minimo').val(resultado.IMP_MAX);

				}else if(resultado.DES.substr(0,5)+" ..." =='Mayor ...'){

					$('#val_maximo').val(resultado.IMP_MIN);


				}else if(resultado.DES.substr(0,5)+" ..." =='Entre ...'){

					$('#val_minimo').val(resultado.IMP_MIN);

					$('#val_maximo').val(resultado.IMP_MAX);

				}


				$('#pormil').val(resultado.POR);

				$('#input_hidden_codigo').val(resultado.COD);
				$('#input_denominacion_nueva').val(resultado.DES);


			}
		});

	});

	$('#boton_modal').click(function(){

			//$('#myModal').modal('toogle'); 
			//$('#input_denominacion_nueva').focus(); 


		});

	$('#guardar_cambios_modal').click(function(){

		var codigo_valor_fiscal=$('#input_hidden_codigo').val();
		var nueva_descripcion=$('#input_denominacion_nueva').val();

		
		$.ajax({
			url: '?c=ListadoActos&a=updateDescripcionValorFiscalAJAX',
			type: 'POST',
			dataType: 'json',
			data: {
				codigo_valor_fiscal: codigo_valor_fiscal,
				nueva_descripcion: nueva_descripcion
			},
			success: function (resultado) {

			
				$('#id_edicion').toggle();	
				
				setTimeout(function(){

						$('#id_edicion').slideToggle('slow');	

				},3000);

			}
		});


		$('#desc_fiscal option[value="'+codigo_valor_fiscal+'"]').text(nueva_descripcion);
		$('#desc_fiscal option[value="'+codigo_valor_fiscal+'"]').attr('selected','selected');

		$('#myModal').modal('hide'); 

		 //location.reload(true);

		});


	
	$('#btn-modificar').click(function(){

		
		limpiar_errores();

		if($('#modificar_resto_valores').is(':checked')){
 		
			
			if(confirm("Esta seguro que desea modificar todos los valores restantes?") == true)
			{
				modificar_con_valores_anteriores();
				pintar_select_desc_fiscal_segun_decreto();
			}

			//console.log("esta checkeado el valor!");
		
		}else{ //en el caso de no estar checkeado "Modificar los valores restantes"

			
			if(validarFormModificacion()){


			$.ajax({
			url: '?c=ListadoActos&a=modificarDescripcionFiscal',
			type: 'POST',
			dataType: 'json',
			data: {
				desc_fiscal: $('#desc_fiscal').val(),
				val_minimo: $('#val_minimo').val(),
				val_maximo: $('#val_maximo').val(),
				importe_minimo: $('#importe_minimo').val(),
				importe_maximo: $('#importe_maximo').val(),
				pormil: $('#pormil').val(),
				decreto_numero: $('#decreto_numero').val(),
				fecha_entrada: $('#fecha_entrada').val(),
				fecha_en_vigencia: $('#fecha_vigencia').val()
			},
			success: function (resultado) {
				

				$('#id_modificacion').slideToggle();	
				
				setTimeout(function(){

					$('#id_modificacion').slideToggle('slow');	

				},3000);

				$('#desc_fiscal option[value="'+resultado.COD+'"]').css('font-weight','bold');

				$('#desc_fiscal option[value="'+resultado.COD+'"]').text(castear_descripcion(resultado));
				$('#desc_fiscal option[value="'+resultado.COD+'"]').attr('selected','selected');

			}
		});

		}

		}



	});


function verificar_hasta_entre_mayor(){


	if($('#desc_fiscal').val() == 3){

		$('#limite_superior').show("slow");
		$('#limite_inferior').hide("slow");
		$('#boton_modal').hide();

	}else if($('#desc_fiscal').val() == 2){

		$('#limite_superior').show("slow");
		$('#limite_inferior').show("slow");
		$('#boton_modal').hide();


	}else if($('#desc_fiscal').val() == 1){

		$('#limite_superior').hide("slow");
		$('#limite_inferior').show("slow");
		$('#boton_modal').hide();



	}else{

		$('#limite_superior').hide("slow");
		$('#limite_inferior').hide("slow");
		$('#boton_modal').show();


	}

}


function comprobar_pertenece_numero_decreto_AJAX(codigo_desc_fiscal, texto_descricion, objeto){
	

		$.ajax({
			url: '?c=ListadoActos&a=buscarSiPerteneceNumeroDecretoAjax',
			type: 'POST',
			dataType: 'json',
			data: {
				desc_fiscal: codigo_desc_fiscal,
				decreto_numero: $('#decreto_numero').val()
			},
			success: function (resultado) {
				
			if(resultado.PERTENECE){
				objeto.css({'font-weight':'bold'});
			}

		}
		});

	return window.pertenece;
}

function pintar_select_desc_fiscal_segun_decreto(){

		$('#desc_fiscal').children().each(

        function (){
        	 
        	 comprobar_pertenece_numero_decreto_AJAX( $(this).val() , $(this).text(), $(this) );

             // if(comprobar_pertenece_numero_decreto_AJAX( $(this).val() ) ){
             //     $(this).css({'background':'red'});
             // }
        }
);

}

function modificar_con_valores_anteriores(){

		$.ajax({
			url: '?c=ListadoActos&a=modificarConValoresAnteriores',
			type: 'POST',
			dataType: 'json',
			data: {

				decreto_numero: $('#decreto_numero').val(),
				fecha_entrada: $('#fecha_entrada').val(),
				fecha_en_vigencia: $('#fecha_vigencia').val()
			},
			success: function (resultado) {
				

		}
		});
	
}


function limpiar_lista(){

	$('#desc_fiscal').children().each(

        function (){
        	 
		$(this).css({'font-weight':'normal'});
        
        }
)}


});
