$( document ).ready(function() {


	$('#btn-buscar-decreto').click(function(){

		$('#div_buscar_por_decreto').toggle("slow");

	});


	$('#btn-buscar-fecha').click(function() {

		limpiar_errores();

		var startTime =moment($('#fecha_desde').val()).format("DD/MM/YYYY");

		var end =moment($('#fecha_hasta').val()).format("DD/MM/YYYY");
		
		if(validarFechas())
		{

			$.ajax({
				url:'?c=ListadoActos&a=obtenerValoresFiscalesEntreFechasAJAX',
				type: 'POST',
				dataType: 'json',
				data: {
					fecha_desde: startTime,
					fecha_hasta: end,
				},
				success: function (res) {

					$("#tabla_decreto_por_fechas thead").remove();
					$("#tabla_decreto_por_fechas tbody").remove();

					$("#tipo_tabla").remove();

					var lista_decretos=[];


					var head_tabla ="<tr>"
					+		"<th style='text-align: center;'>Decreto Nº</th>"
					+		"<th style='text-align: center;'>Fecha en vigencia</th>"

					+"</tr>";


					$('#tabla_decreto_por_fechas').append(head_tabla);


					for (i=0; i<res.length; i++){

						var fecha=res[i].FEC_EN_VIGENCIA ? moment(res[i].FEC_EN_VIGENCIA).format("DD/MM/YYYY") : "";
						var nro_decreto= res[i].NRO_DECRETO ? res[i].NRO_DECRETO : "";


						//compruebo si el nro de decreto se encuentra en el arreglo, para no repetirlo
						//al mostrarlo en la tabla, si es así, continuo con la próxima iteración.		
						if( _.contains(lista_decretos, res[i].NRO_DECRETO) ) continue;

						if(nro_decreto == "") continue;
						
						//agrego el nro_decreto al arreglo
						lista_decretos.push(res[i].NRO_DECRETO);

		  var body_tabla="<tr>"
						+"<td style='vertical-align: middle; text-align: center;'>"
						+ nro_decreto
						+"</td>"
						+"<td style='vertical-align: middle; text-align: center;'>"
						+ fecha
						+"</td>"
						+"<tr>";


						$("#tabla_decreto_por_fechas tbody").append(body_tabla);

						if($('#tipo_tabla').val() != undefined )
							$('#tipo_tabla').val("tabla_fechas");
						else 
							$("#tabla_decreto_por_fechas").append("<input type='hidden' id='tipo_tabla' value='tabla_fechas' />");


	         }//end for
	     	}//end success ajax


	     });

		}

		});


	$('#btn-search-valor-fiscal').click(function() {


	limpiar_errores();


	if(validarBuscarPorDecreto())
	{	
		$.ajax({
			url:'decreto/buscar/nrodecreto',
			type: 'POST',
			dataType: 'json',
			data: {
				numero_decreto: $('#numero_decreto').val(),
			},
			success: function (resultado) {

				$("#tabla_decreto_por_fechas thead").remove();
				$("#tabla_decreto_por_fechas tbody").remove();

				var head_tabla ="<tr>"
				+		"<th style='text-align: center;'>Descripción</th>"
				+		"<th style='text-align: center;'>Fecha en vigencia</th>"
				+		"<th style='text-align: center;'>Minimo</th>"
				+		"<th style='text-align: center;'>Máximo</th>"
				+		"<th style='text-align: center;'>Pormil (.%)</th>"
				+"</tr>";

				$('#tabla_decreto_por_fechas').append(head_tabla);


				for (i=0; i<resultado.length; i++){

	          
	          var fecha= resultado[i].FEC_EN_VIGENCIA ? moment(resultado[i].FEC_EN_VIGENCIA).format("DD/MM/YYYY") :"";
	          var nro_decreto =resultado[i].NRO_DECRETO ? resultado[i].NRO_DECRETO : "";
	          
	          var minimo = resultado[i].MNI ? resultado[i].MNI : "";
	          var maximo = resultado[i].MMO ? resultado[i].MMO : "";
	          
	          var importe_minimo = resultado[i].IMP_MIN ? resultado[i].IMP_MIN : "";
	          var importe_maximo = resultado[i].IMP_MAX ? resultado[i].IMP_MAX : "";

	          var body_tabla="<tr>"
	          +"<td style='vertical-align: middle; text-align: center;'>"
	          + castear_descripcion(resultado[i])
	          +"</td>"
	          +"<td style='vertical-align: middle; text-align: center;'>"
	          + fecha
	          +"</td>"
	          +"<td style='vertical-align: middle; text-align: center;'>"
	          + minimo
	          +"</td>"
	          +"<td style='vertical-align: middle; text-align: center;'>"
	          + maximo
	          +"</td>"
	          +"<td style='vertical-align: middle; text-align: center;'>"
	          + resultado[i].POR
	          +"</td>"
	          +"<tr>";


	          $("#tabla_decreto_por_fechas tbody").append(body_tabla);

	          if($('#tipo_tabla').val() != undefined )
	          	$('#tipo_tabla').val("tabla_nro_decreto");
	          else 
	          	$("#tabla_decreto_por_fechas").append("<input type='hidden' id='tipo_tabla' value='tabla_nro_decreto' />");

	          
	         }//end for
	     	}//end success ajax


	     });
	}

	});

	$('#btn-imprimir').click(function(){


		if($('#tipo_tabla').val() =='tabla_nro_decreto')
		{

			$('#nro_decreto_formulario').val( $('#numero_decreto').val() );

			$('#form_pdf_nro_decreto').submit();

		}
		else
		{
			$('#fecha_desde_formulario').val( moment($('#fecha_desde').val()).format("DD/MM/YYYY") );
			$('#fecha_hasta_formulario').val( moment($('#fecha_hasta').val()).format("DD/MM/YYYY") );

			$('#form_pdf_fechas').submit();

		}
	
	});

});