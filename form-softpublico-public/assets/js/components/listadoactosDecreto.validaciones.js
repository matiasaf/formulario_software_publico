function validarBuscarPorDecreto(){


	global_error=true;


		if($('#numero_decreto').val() ==""){
			
			$('#lista_errores').append('<li id="li-decreto">Número de decreto no ingresado</li>');
			
			//pinto en rojo el label indicando donde se cometio el error
			$('#lbl-nro-decreto').css({'background-color':'#d9534f'});
			
			if( !$('#div_errores').is(":visible") ){

						$('#div_errores').show();
		
			}

			global_error=false;

		}

		if( !isNumerico($('#numero_decreto').val() ))
		{
		
			$('#lista_errores').append('<li id="li-decreto">El Valor ingresado debe ser únicamente Numérico (no debe poseer letras o caracteres distintos a un número)</li>');
			
			//pinto en rojo el label indicando donde se cometio el error
			$('#lbl-nro-decreto').css({'background-color':'#d9534f'});
			
			if( !$('#div_errores').is(":visible") ){

						$('#div_errores').show();
		
			}

			global_error=false;

		}


	return global_error;

}

function validarFechas(){

	global_error=true;

	var startTime =moment($('#fecha_desde').val());

	var end =moment($('#fecha_hasta').val());
		
		
		if(startTime.isAfter(end))
		{
			$('#lista_errores').append('<li id="li-decreto">Debe ingresar un intervalo de tiempo correcto (La Fecha hasta no puede ser anterior a la Fecha desde)</li>');
			
			//pinto en rojo el label indicando donde se cometio el error
			$('#lbl-fecha-desde').css({'background-color':'#d9534f'});
			$('#lbl-fecha-hasta').css({'background-color':'#d9534f'});

			if( !$('#div_errores').is(":visible") ){

						$('#div_errores').show();
		
			}

			global_error=false;


		}

		
	
	return global_error;
}

function limpiar_errores(){

	// console.log("el div de error esta visibile : "+$('#div_errores').is(":visible"));

	$('#div_errores').hide();

	//seteo en grises los labels
	$('#lbl-nro-decreto').css({'background-color':'#777'});
	$('#lbl-fecha-desde').css({'background-color':'#777'});
	$('#lbl-fecha-hasta').css({'background-color':'#777'});

	$("#lista_errores").empty();
	$("#tabla_decreto_por_fechas").empty();

}


function isNumerico(nro_decreto){
 
 var code,i;

 	for (i = 0; i < nro_decreto.length; i++) {
    
    code = nro_decreto.charCodeAt(i);
    
    if (!(code > 47 && code < 58)) {  // numeric (0-9) 
      return false;
    }
  }

  return true;
}

