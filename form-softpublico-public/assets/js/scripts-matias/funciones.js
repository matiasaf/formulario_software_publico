  
//funciones globales de validacion y demases

function castear_descripcion(descripcion){

	var descripcion_valor_fiscal= descripcion.DES;

	if(descripcion.DES=="hasta ..." ) 

		descripcion_valor_fiscal= descripcion.DES.substr(0,5) + ' $' + dar_formato_numero(descripcion.IMP_MAX);

	if(descripcion.DES=="Entre ..."  )

		descripcion_valor_fiscal= descripcion.DES.substr(0,5) + ' $' + dar_formato_numero(descripcion.IMP_MIN) +' y $' + dar_formato_numero(descripcion.IMP_MAX);

	if(descripcion.DES=="Mayor ..."  )

		descripcion_valor_fiscal= descripcion.DES.substr(0,5) + ' a $' + dar_formato_numero(descripcion.IMP_MIN);


	return descripcion_valor_fiscal;

}


function dar_formato_numero(numero){

	var resultado = numero.length == 6 
	? numero.substr(0,3) + '.' + numero.substr(3,3)
	: numero.length == 7
	? numero.substr(0,1) + '.' + numero.substr(1,3) + '.' + numero.substr(4,3)
	: '';

	return resultado;

}



