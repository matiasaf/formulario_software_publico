
@extends('layouts.app')

@section('estilos')
<style type="text/css">

#boton_enviar{

-webkit-box-shadow: 1px 0px 25px 0px rgba(0,0,0,0.46);
-moz-box-shadow: 1px 0px 25px 0px rgba(0,0,0,0.46);
box-shadow: 1px 0px 25px 0px rgba(0,0,0,0.46);

}

.font-error{

	color: #9f3a38;

}

body{

	background-color:#dadada;

}

.img-nav{

	position: relative;
	width: 100%;
	height: 250px;

}

.hay-error{
	
	border-color: #9f3a38;

}



</style>


@endsection

@section('content')

<img class="img-nav" src="assets/img/codigo.jpg"></img>

<div class="ui container" id="manage-vue">	

	<div class="ui menu">
	  
	  <div class="right menu">
	  
	     @if(Auth::guest())
	    
	      <div class="item">
	        {{-- <a @click="setModalOn" ><div class="ui primary button">Log-in</div></a> --}}
	       <a href="{{route('auth/login')}}"> <div class="ui primary button">Acceder</div></a>
	      </div>
	    
	    @else

		   <div class="item">
				Bienvenido/a {{ Auth::user()->name }}
		   </div>
	       <div class="item">
	          <a href="{{route('auth/logout')}}"><div class="ui primary button">Cerrar sesión</div></a>
	       </div>

	    @endif
	  
	  </div>
	 
	</div>


	<div class="ui four cards">
	  
	  <div class="card" v-for="aplicacion in all_aplicaciones">
	    <div class="content">
	      <div class="header">
	       @{{aplicacion.nombre_aplicacion}} - versión @{{aplicacion.version_sof}}
	      </div>
	      <div class="meta">
	       Lenguaje : @{{aplicacion.lenguaje_nom}}
	      </div>
	      <div class="description">
	        @{{aplicacion.descripcion}}
	      </div>
	    </div>
	    <div class="extra content">
	      <div class="ui two buttons">
	        <button @click="modificarAplicacion(aplicacion)" class="ui basic green button">Modificar</button>
	        <button @click="eliminarAplicacion(aplicacion)" class="ui basic red button">Eliminar</button>
	      </div>
	    </div>
	  </div>

	</div>

	<br>
	<br>

	 <a href="{{route('index')}}"><div style="margin-left: 40%;" class="ui green button">Volver a la encuesta</div></a>



<template v-if="habilitar_form_edicion">

			<div class="ui form">

					<div class="ui grid">

  						<div class="one column row">
						    <div class="centered floated column">

						    	<div class="field" v-bind:class="{ 'error': errors.has('input_nombre_aplicacion') || errores.nombre_aplicacion }">
									<label>Nombre de la aplicación :</label>
									<input v-validate="'required'" v-on:keyup="limpiarNombreAplicacion" type="text" placeholder="" name="input_nombre_aplicacion" v-model="aplicacion_seleccionada.nombre_aplicacion" />
									<template v-if="errores.nombre_aplicacion">
										<label class="error" v-for="error in errores.nombre_aplicacion">@{{error}}</label>
									</template>
									<template v-else>
										<label class="error" v-show="errors.has('input_nombre_aplicacion')">@{{errors.first('input_nombre_aplicacion')}}</label>
									</template>
								</div>

						    </div>
					  	</div>

  						<div class="one column row">
						    
						    <div class="centered floated column">

						    	<div class="field" v-bind:class="{ 'error': errors.has('input_version_software') || errores.input_version_software }">
									<label>¿Versión actual de la aplicación?</label>
									<input v-validate="'required'" v-on:keyup="limpiarVersionSoftware" type="text" placeholder="" name="input_version_software" v-model="aplicacion_seleccionada.version_sof" />
									<template v-if="errores.version_sof">
										<label class="error" v-for="error in errores.version_sof">@{{error}}</label>
									</template>
									<template v-else>
										<label class="error" v-show="errors.has('input_version_software')">@{{errors.first('input_version_software')}}</label>
									</template>

								</div>

						    </div>
					  	</div>


					  <div class="one column row">
					    <div class="centered floated column">

					    	<div class="field" v-bind:class="{ 'error': errors.has('input_descripcion_software') || errores.descripcion }">
								
								<label>Breve descripción / ¿Qué es lo que hace?</label>
								<textarea v-validate="'required'" v-on:keyup="limpiarDescripcion" placeholder="Descripción :" name="input_descripcion_software" v-model="aplicacion_seleccionada.descripcion"></textarea>
								
								<template v-if="errores.descripcion">
										<label class="error" v-for="error in errores.descripcion">@{{error}}</label>
								</template>
								<template v-else>
										<label class="error" v-show="errors.has('input_descripcion_software')">@{{errors.first('input_descripcion_software')}}</label>
								</template>
							
							</div>

					    </div>
					  </div>



					  <div class="two column row">

					    <div class="left floated column">

					    	{{-- <div class="field" v-bind:class="{ 'error': errors.has('input_lenguaje_programacion'), 'disabled': save_form_software }">

						      <label>Lenguaje de Programación </label>
						      <input  v-model="input_lenguaje_programacion" name="input_lenguaje_programacion" v-validate="'required'" placeholder="Lenguaje de programación:" type="text">

						      <label v-show="errors.has('input_lenguaje_programacion')" class="error"> @{{ errors.first('input_lenguaje_programacion')}}</label>

						    </div> --}}

 									  <template v-if="!otro_lenguaje">

								    	<div class="field" v-bind:class="{ 'error': errors.has('input_lenguaje_programacion') || errores.lenguaje_nom }">
									      
									      <label>Lenguaje de Programación</label>

									      <select v-validate="'required'" v-model="aplicacion_seleccionada.lenguaje_nom" name="input_lenguaje_programacion">
									      	
									      	<option value="" selected=""></option>
									      	<option v-for="lenguaje in all_lenguajes"  :value="lenguaje"> @{{ lenguaje }}</option>

									      </select>
										
										  <label class="error" v-show="errores.lenguaje_nom"> @{{errores.lenguaje_nom}} </label>
										  <label class="error" v-show="errors.has('input_lenguaje_programacion')">@{{errors.first('input_lenguaje_programacion')}}</label>

									    </div>

									  </template>

									  <template v-else>
									  	
									  	<div class="field" >
									      
									      <label>Lenguaje de Programación</label>

									  {{--   <v-select v-model="input_tipo_licencia" :options="all_licencias">
									    </v-select> --}}

									      <select  v-validate="'required'" v-model="input_lenguaje_programacion" name="input_lenguaje_programacion">
									      	
									      	<option value="" selected=""></option>
									      	<option v-for="lenguaje in all_lenguajes" > @{{ lenguaje }}</option>

									      </select>
									     

									    </div>

									  	<div class="field" v-bind:class="{'error': errors.has('input_lenguaje_programacion') || errores.lenguaje_nom }">

									   	 <label>Ingrese el lenguaje</label>

									     <input @keyup="limpiarLenguajeNombre" v-validate="'required'" type="text" v-model="aplicacion_seleccionada.lenguaje_nom" placeholder=""  />


									      <label class="error" v-show="errores.lenguaje_nom"> @{{errores.lenguaje_nom}} </label>
										  <label class="error" v-show="errors.has('input_lenguaje_programacion')">@{{errors.first('input_lenguaje_programacion')}}</label>

									    </div>

									  </template>


					    </div>

					    <div class="right floated column">

					    	<div class="field" v-bind:class="{ 'error': errors.has('input_lenguaje_programacion_version') || errores.lenguaje_ver }">

						      <label>Versión del lenguaje</label>

						      <input @keyup="limpiarLenguajeVersion" v-model="aplicacion_seleccionada.lenguaje_ver" name="input_lenguaje_programacion_version" v-validate="'required'" placeholder="Versión:" type="text">
									      
							  <label class="error" v-show="errores.lenguaje_ver"> @{{errores.lenguaje_ver}} </label>
						      <label v-show="errors.has('input_lenguaje_programacion_version')" class="error"> @{{ errors.first('input_lenguaje_programacion_version')}}</label>

						    </div>

					    </div>

					  </div>

					  
					  <div class="two column row">

					    <div class="left floated column">

					    	<div class="field" v-bind:class="{ 'error': errors.has('version_mobile')}">

								<div class="ui slider checkbox" style="text-align:center; ">
								  <input type="checkbox" v-model="aplicacion_seleccionada.version_mobile" name="newsletter">
								  <label>¿Tiene versión mobile?</label>
								</div>

						   </div>

					    </div>

					    <div class="right floated column">

					    	<div class="field" v-bind:class="{ 'error': errors.has('version_web') }">

						     	<div class="ui slider checkbox">

								  <input type="checkbox" v-model="aplicacion_seleccionada.version_web" name="newsletter">
								  <label>¿Posee versión web?</label>

								</div>
						    </div>

					    </div>

					  </div>


					  <div class="two column row">

					    <div class="left floated column">


						      <template v-if="!otra_db">

								    	<div class="field" v-bind:class="{ 'error': errors.has('input_base_dato')}">
									      
									      <label>Base de datos asociada</label>

									      <select v-validate="'required'" v-model="aplicacion_seleccionada.base_nom" name="input_base_dato">
									      	
									      	<option value="" selected=""></option>
									      	<option v-for="db in all_dbs"  :value="db"> @{{ db }}</option>

									      </select>

									    </div>

									  </template>

									  <template v-else>
									  	
									  	<div class="field" v-bind:class="{'error': errors.has('input_base_dato')}">
									      
									      <label>Base de datos asociada</label>

									      <select v-validate="'required'" v-model="aplicacion_seleccionada.base_nom" name="input_base_dato" >
									      	
									      	<option value="" selected=""></option>
									      	<option v-for="db in all_dbs" > @{{ db }}</option>

									      </select>

									    </div>

									  	<div class="field" v-bind:class="{ 'error': errores.base_nom || errors.has('input_base_dato_otra') }">

									   	 <label>Ingrese la base de datos</label>

									     <input @keyup="limpiarBaseDatosNombre" v-validate="'required'" type="text" v-model="input_base_dato_otra" name="input_base_dato_otra" placeholder=""  />

							 			 <label class="error" v-show="errores.base_nom"> @{{errores.base_nom}} </label>
						      			 <label v-show="errors.has('input_base_dato_otra')" class="error"> @{{ errors.first('input_base_dato_otra')}}</label>

									    </div>

									  </template>

					    </div>

					    <div class="right floated column">

					    	<div class="field" v-bind:class="{ 'error': errors.has('input_base_dato_version')}">

						      <label>Versión de base de dato</label>

						      <input  v-model="aplicacion_seleccionada.base_ver" name="input_base_dato_version" v-validate="'required'" placeholder="Versión (BD):" type="text">

						      <label v-show="errors.has('input_base_dato_version')" class="error"> @{{ errors.first('input_base_dato_version')}}</label>

						    </div>

					    </div>

					  </div>

			
							  <div class="one column row">
							    <div class="centered floated column">

							    	<div class="field" v-bind:class="{ 'error' : errors.has('input_pasos_instalacion') || errores.instalacion  }">
										
										<label>Indique los pasos para su instalación</label>

										<textarea @keyup="limpiarCampoInstalacion" v-model="aplicacion_seleccionada.instalacion" placeholder="" name="input_pasos_instalacion" ></textarea>

										<template v-if="errores.instalacion">
											
											<label class="error" v-for="error in errores.instalacion"> @{{error}} </label>

										</template>
										<template v-else>
											
											<label v-show="errors.has('input_pasos_instalacion')" class="error"> @{{ errors.first('input_pasos_instalacion')}}</label>

										</template>

									</div>

							    </div>
							  </div>



							  <div class="one column row">
							    <div class="centered floated column">

							    	<div class="field" v-bind:class="{'error': errores.reparticion}" id="reparticion">
								      
								      <label>Repartición donde se encuentra instalado</label>
								     
								      <input  @keyup="limpiarReparticion" name="input_reparticion" v-model="aplicacion_seleccionada.reparticion" placeholder="Ej: Reparticion1 ; Reparticion2 ; Reparticion3 (separados por punto y coma en el caso de ser más de una). " type="text">
								    
								      <template v-if="errores.reparticion">
								      	
								      		<label class="error" v-for="error in errores.reparticion"> @{{error}} </label>

								      </template>
								      <template v-else>
								      
								      	 <label class="error" v-show="errors.has('input_reparticion')"> @{{ errors.first('input_reparticion') }} </label>

								      </template>
							 		  
							 		  <div class="ui special popup" id="popup_reparticion">
  										<div>Se refiere a otras áreas u organismos que utilicen esta aplicación.</div>
									  </div>

								    </div>


							    </div>
							  </div>



								 <div class="one column row">
								    
								    <div class="centered floated column">

								    	<div class="field" v-bind:class="{'error': errores.input_tipo_licencia}">
									      
									      <label>Personas responsables </label>

									  {{--   <v-select v-model="input_tipo_licencia" :options="all_licencias">
									    </v-select> --}}

											<table class="ui selectable celled table">
											  <thead>
											    <tr>
											      
											      <th>Nombre</th>
											      <th>Correo</th>
											      <th>Celular</th>
											      <th>Telefono</th>
											      <th><button @click="habilitarAgregarNuevoResponsable"><i class="fa fa-user-plus" aria-hidden="true"></i></button></th>
											    
											    </tr>
											  </thead>
											  <tbody>
											    <tr v-for="responsable in aplicacion_seleccionada.responsables">
											      
											      <template v-if="!responsable.habilitar_edicion">
												      <td>@{{responsable.responsable}}</td>
												      <td>@{{responsable.correo}}</td>
												      <td>@{{responsable.celular}}</td>
												      <td>@{{responsable.telefono}}</td>
												      <td><button @click="habilitarEdicion(responsable)"><i class="fa fa-pencil-square" aria-hidden="true"></i></button><button @click="eliminarResponsable(responsable)"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
												  </template>

											      <template v-else>
												    
												    <td>
												      
												      <div class="field" v-bind:class="{'error': errores.responsable}">
												      
												      <input v-on:keyup="limpiarCampoResponsable" type="text" v-model="responsable.responsable" />

												      <template v-if="errores.responsable">
									
														<label class="error" v-for="error in errores.responsable">@{{error}}</label>

													  </template>

												      </div>

												    </td>
												      
												    <td>

												     <div class="field" v-bind:class="{'error': errores.correo}">

												     <input v-on:keyup="limpiarCampoEmail" type="email" v-model="responsable.correo" />

												      <template v-if="errores.correo">
									
														<label class="error" v-for="error in errores.correo">@{{error}}</label>

													  </template>

													  </div>

												    </td>

												      <td><input type="text" v-model="responsable.celular" /></td>
												      <td><input type="text" v-model="responsable.telefono" /></td>
												 	  <td><button @click="confirmarEdicion(responsable)"><i class="fa fa-check-circle" aria-hidden="true"></i></button></td>
												  </template>
											    

											    </tr>

											    {{-- Fila para agregar un nuevo responsable --}}
											    <tr v-if="habilitar_nuevo_responsable">
											     
		    										<td>
												      
												      <div class="field" v-bind:class="{'error': errores.responsable}">
												      
												      <input v-on:keyup="limpiarCampoResponsable" type="text" v-model="responsable_a_agregar_responsable" />

												      <template v-if="errores.responsable">
									
														<label class="error" v-for="error in errores.responsable">@{{error}}</label>

													  </template>

												      </div>

												    </td>

												    <td>

												     <div class="field" v-bind:class="{'error': errores.correo}">

												     <input v-on:keyup="limpiarCampoEmail" type="email" v-model="responsable_a_agregar_correo" />

												      <template v-if="errores.correo">
									
														<label class="error" v-for="error in errores.correo">@{{error}}</label>

													  </template>

													  </div>

												    </td>

											     <td><input type="text" v-model="responsable_a_agregar_celular"></td>
												 <td><input type="text" v-model="responsable_a_agregar_telefono"></td>

												 <td><button @click="agregarNuevoResponsable"><i class="fa fa-check-circle" aria-hidden="true"></i></button></td>

											    </tr>   
											  </tbody>
											</table>
									    </div>

								    </div>

								  </div>




								  <div class="one column row">
								    
								    <div class="centered floated column">

								    	<div class="field" v-bind:class="{'error': errores.licencia}">
									      
									      <label>Tipo de licencia</label>

									  {{--   <v-select v-model="input_tipo_licencia" :options="all_licencias">
									    </v-select> --}}

									      <select v-on:keyup="limpiarLicencia" v-validate="'required'" v-model="aplicacion_seleccionada.licencia" name="input_tipo_licencia">
									      	
									      	<option value="" selected=""></option>
									      	<option v-for="licencia in all_licencias" > @{{ licencia }}</option>

									      </select>

									      <template v-if="errores.licencia">
									      	
									      	<label class="error" v-for="error in errores.licencia"> @{{error}} </label>

									      </template>


									    </div>

								    </div>

								  </div>


								 <div class="one column row">
								    <div class="left floated column">


								    	<div class="field" style="text-align: center;">
											<div class="ui buttons">
												  <button class="ui button">Cancelar</button>
												  <div class="or"></div>
												  <button @click="onGuardarCambiosAplicacion" class="ui blue button active">Guardar cambios</button>
												</div>
										</div>

										

								    </div>
						  		</div>

					</div>
			</div>

</template>

</div>


@endsection


@section('scripts')

{{-- <script type="text/javascript" src="http://closure-compiler.appspot.com/code/jsc81247ddc5c50e240d900582629789a85/default.js" ></script>
 --}}

<script type="text/javascript">


  Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr('value');

  Vue.use(VeeValidate);
  Vue.component('v-select', VueSelect.VueSelect);


  var vm = new Vue({

    el: '#manage-vue',


    data: function(){

      return {

      	aplicacion_seleccionada: {},
    	all_aplicaciones : [],

		input_base_dato_otra : '',

    	responsable_a_agregar_responsable: '',
    	responsable_a_agregar_correo: '',
    	responsable_a_agregar_telefono: '',
    	responsable_a_agregar_celular: '',
       	
       	habilitar_nuevo_responsable: false,

 		 all_lenguajes: ['ABAP2','ABL3','ActionScript',
		'ActionScript 3','C Sharp (C#)','Clarion','Clipper','D',
		'Object Pascal (Embarcadero Delphi)','Gambas','GObject','Genie','Harbour',
		'Eiffel','Fortran 90/95','Java','JavaScript','Lexico','Objective-C',
		'Ocaml','Oz','R','Pascal','Perl','PHP','PowerBuilder','Processing','Python','Ruby','Self',
		'Smalltalk','Magik','Vala','VB.NET','Visual FoxPro','Visual Basic 6.0','Visual DataFlex',
		'Visual Objects','XBase++','DRP','Scala',
		'Otro ...'
		],

		all_dbs : ['DB2','Firebird','HSQL','Informix','Interbase','MariaDB','Microsoft SQL Server','MySQL','Oracle','PostgreSQL','PervasiveSQL','SQLite','Sybase ASE', 'Otra ...'],

	    all_licencias : [

		'Academic Free License','Apache License','Apple Public Source License','Artistic License','Berkeley Database License','BSD license',
		'Boost Software License','Common Development and Distribution','Common Public License','Creative Commons Licenses',
		'Cryptix General License','Eclipse Public License','Educational Community License','Eiffel Forum License',
		'GNU General Public License','GNU Lesser General Public License','Hacktivismo Enhanced-Source Software License Agreement',
		'IBM Public License','Intel Open Source License','ISC license Internet Systems','LaTeX Project Public License',
		'MIT license / X11 license','Mozilla Public License','Netscape Public License','Open Software License',
		'OpenSSL license','PHP License','Poetic License','Python Software','Q Public License','Sun Industry Standards Source License','Sun Public License',
		'Sybase Open Watcom','W3C Software Notice and License','XFree86 1.1 License','Licencia zlib/libpng','Zope Public License',
		' '

	    ],

       	errores:[],

       	habilitar_form_edicion: false,

        path: window.location.href.slice(0,-17)


      }

    },


    methods: {


      getAplicaciones: function(){

      this.$http.get( this.path +'/aplicaciones/usuario' ).then((response) =>{

          this.$set(this,'all_aplicaciones', response.data);

        }, (response) => {


        });

      },

     modificarAplicacion: function(aplicacion){

     	this.aplicacion_seleccionada = aplicacion;

     	this.habilitar_form_edicion = true;

     },

     eliminarAplicacion: function(aplicacion){
     	
     	this.$http.post(this.path + '/aplicacion/delete' ,
     	{
     		id: aplicacion.id
     	
     	}).then((response) => {

     		toastr.success(response.data.message, '¡Operación exitosa!', {timeOut: 5000});
     		this.getAplicaciones();

     	}, (response) => {


     	});

     },

     onGuardarCambiosAplicacion: function(){
    
    	var posee_codigo_fuente = this.aplicacion_seleccionada.cod_fuente ? 'si' : 'no';

		var nombre_base_datos = this.otra_db ? this.input_base_dato_otra : this.aplicacion_seleccionada.base_nom;

     	this.$http.post(this.path + '/aplicacion/edit' ,
     	{
     		id: this.aplicacion_seleccionada.id,

     		nombre_aplicacion : this.aplicacion_seleccionada.nombre_aplicacion,
  	  		version_sof : this.aplicacion_seleccionada.version_sof,
  	  		descripcion : this.aplicacion_seleccionada.descripcion,
  	  		lenguaje_nom : this.aplicacion_seleccionada.lenguaje_nom,
  	  		lenguaje_ver : this.aplicacion_seleccionada.lenguaje_ver,
  	  		version_mob : this.aplicacion_seleccionada.version_mob,
  	  		version_web : this.aplicacion_seleccionada.version_web,
  	  		base_nom  : nombre_base_datos,
  	  		base_ver : this.aplicacion_seleccionada.base_ver,

  	  		instalacion: this.aplicacion_seleccionada.instalacion,

  	  		desa_por_emp: this.aplicacion_seleccionada.desa_por_emp,
  	  		empresa_nom: this.aplicacion_seleccionada.empresa_nom,
  	  		cod_fuente: posee_codigo_fuente,
  	  		repositorio: this.aplicacion_seleccionada.repositorio,

  	  		reparticion: this.aplicacion_seleccionada.reparticion,

  	  		// responsables : this.aplicacion_seleccionada.responsables,

  	  		licencia: this.aplicacion_seleccionada.licencia

     	
     	}).then((response) => {

     		toastr.success(response.data.message, '¡Operación exitosa!', {timeOut: 5000});
     		this.getAplicaciones();

     	}, (response) => {

     		toastr.error('Debe revisar los datos al modificar la aplicación.', '¡Hubo errores!', {timeOut: 5000});

     		this.$set(this,'errores',response.data);

     	});

     },

     habilitarEdicion: function(responsable)
     {
     	Vue.set(responsable,'habilitar_edicion',true);
     },

     eliminarResponsable : function(responsable)
     {
     	this.$http.post( this.path + '/responsables/delete',
     	{     		
     		id: responsable.id

     	}).then( (response) => {

     		toastr.success('Usuario eliminado con éxito.', '¡Operación exitosa!', {timeOut: 5000});

        	this.aplicacion_seleccionada.responsables = this.aplicacion_seleccionada.responsables.filter(item => item.id !== responsable.id);


     	}, (response) => {

     		toastr.error('Problemas al eliminar.', '¡Hubo error!', {timeOut: 5000});

     	});

     },

     habilitarAgregarNuevoResponsable: function()
     {
     	this.habilitar_nuevo_responsable = true;
     },

     agregarNuevoResponsable : function()
     {
     	this.$http.post( this.path + '/responsables/add',
     	{
     		responsable: this.responsable_a_agregar_responsable,
     		correo: this.responsable_a_agregar_correo,
     		telefono: this.responsable_a_agregar_telefono,
     		celular: this.responsable_a_agregar_celular,
     		app_id: this.aplicacion_seleccionada.id

     	}).then( (response) => {

     		toastr.success('Usuario agregado con éxito.', '¡Operación exitosa!', {timeOut: 5000});

     		this.aplicacion_seleccionada.responsables.push(
     		{
     			id: response.data.id,
     			responsable: this.responsable_a_agregar_responsable,
	     		correo: this.responsable_a_agregar_correo,
	     		telefono: this.responsable_a_agregar_telefono,
	     		celular: this.responsable_a_agregar_celular

     		});

     		this.habilitar_nuevo_responsable = false;

	     	this.responsable_a_agregar_responsable = '';
	     	this.responsable_a_agregar_correo = '';
	     	this.responsable_a_agregar_celular = '';
	     	this.responsable_a_agregar_telefono = '';

     	}, (response) => {

     		toastr.error('Problemas al agregar el usuario.', '¡Hubo error!', {timeOut: 5000});
     		
     		this.$set(this, 'errores', response.data)

     	});

     },

     confirmarEdicion : function(responsable)
     {
	
	     	this.$http.post( this.path + '/responsables/edit',
	     	{
	     		id: responsable.id,
	     		responsable: responsable.responsable,
	     		correo: responsable.correo,
	     		telefono: responsable.telefono,
	     		celular: responsable.celular
	     		
	     	}).then( (response) => {

	     		toastr.success('Usuario agregado con éxito.', '¡Operación exitosa!', {timeOut: 5000});

	     		Vue.set(responsable, 'habilitar_edicion', false);


	     	}, (response) => {

	     		this.$set(this,'errores', response.data);

	     		toastr.error('Problemas al agregar el usuario.', '¡Hubo error!', {timeOut: 5000});

	     	});

     },

     limpiarNombreAplicacion : function(){

     	this.errores.nombre_aplicacion = '';
     },

     limpiarDescripcion: function(){

     	this.errores.descripcion = '';
     },

     limpiarVersionSoftware: function(){

     	this.errores.version_sof = '';
     },

     limpiarReparticion: function(){

     	this.errores.reparticion = '';
     },

     limpiarCampoInstalacion: function(){

     	this.errores.instalacion = '';
     },

     limpiarCampoResponsable : function(){

     	this.errores.responsable = '';
     },

     limpiarCampoEmail : function(){

     	this.errores.correo = '';
     },

     limpiarLicencia : function(){

     	this.errores.licencia = '';
     }

    
    },


    computed: {

    	otra_licencia(){

				if(this.input_tipo_licencia == 'Otra ...') 	return true;

    	},

    	otro_lenguaje(){

				if(this.input_lenguaje_programacion == 'Otro ...') return true;

    	},

    	otra_db(){

				if(this.aplicacion_seleccionada.base_nom == 'Otra ...') return true;

    	}

    },

    mounted : function (){

    	this.getAplicaciones();

    }

  });

vm.$watch('aplicacion_seleccionada.licencia', function(){

	this.errores.licencia = '';

});

</script>


@endsection
