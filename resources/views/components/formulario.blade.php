
@extends('layouts.app')

@section('estilos')
<style type="text/css">

#boton_enviar{

-webkit-box-shadow: 1px 0px 25px 0px rgba(0,0,0,0.46);
-moz-box-shadow: 1px 0px 25px 0px rgba(0,0,0,0.46);
box-shadow: 1px 0px 25px 0px rgba(0,0,0,0.46);

}

.font-error{

	color: #9f3a38;

}

body{
	background-color:#dadada;
}

.img-nav{

	position: relative;
	width: 100%;
	height: 250px;

}

.font-concretada-carga{

	font-family: 'Slabo 27px', serif;

	font-size: 20px;
    margin-top: 1%;
    margin-bottom: 1%;
}

.holder_boton_guardar_cancelar{

	margin:auto;
	margin-top: 2%;

}


</style>


@endsection


@section('content')


<img class="img-nav" src="assets/img/codigo.jpg" >


<div class="ui container" id="manage-vue">	

	<div class="ui menu">
	  
	  @if(!Auth::guest())
	
		  <div class="item">
	      
	       <a href="{{route('aplicaciones.ver_vista_aplicaciones')}}"> <div class="ui primary button">Ver otras aplicaciones cargadas</div></a>
	      
	      </div>	  

	      <div class="item">
	        {{-- <a @click="setModalOn" ><div class="ui primary button">Log-in</div></a> --}}
		       <template v-if="!encuesta_info_general" >
		       
		      	 <a href="{{route('informacion_general.get_view')}}"> <div class="ui red button">Debe cargar la encuesta de información general</div></a>
		       
		       </template>
		       	
		      <template v-else>	       
		      
			      <a href="{{route('informacion_general.get_view')}}"> <div class="ui green button">Revisar la información general ya cargada</div></a>

		      </template>	       

	      </div>

	  @endif

	  <div class="right menu">
	  
	     @if(Auth::guest())
	     
	      <div class="item">
	        {{-- <a @click="setModalOn" ><div class="ui primary button">Log-in</div></a> --}}
	       <a href="{{route('auth/login')}}"> <div class="ui primary button">Acceder</div></a>
	      </div>
	    
	    @else

		    <div class="item">
				Bienvenido/a {{ Auth::user()->name }}
		    </div>
	       <div class="item">
	        <a href="{{route('auth/logout')}}"><div class="ui primary button">Cerrar sesión</div></a>
	      </div>

	    @endif
	  
	  </div>
	 
	</div>

	<br>


@if(Auth::guest())

<div class="ui raised segments">

  <div class="ui segment">
    <p style="text-align: center;">Necesita estar logueado para visualizar el formulario.</p>
  </div>

</div>

	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>

@else

<template v-if="habilitar_ending_page">

	<div class="ui raised segments">


{{-- 	 <div class="ui segment">
	 	
	 	<div class="ui center aligned grid">

	 	<div class="font-concretada-carga" >Nombre de las aplicaciones cargadas.</div>

	 	</div>

	 </div> --}}


{{--   	<div class="ui segment">
  	  
	  <div class="ui four cards">
	  
		  <div class="card" v-for="aplicacion in all_aplicaciones">
		    <div class="content">
		      <div class="header">
		       @{{aplicacion.nombre_aplicacion}}
		      </div>
		      <div class="meta">
		       Lenguaje : @{{aplicacion.lenguaje_nom}}
		      </div>
		      <div class="description">
		        @{{aplicacion.descripcion}}
		      </div>
		    </div>
		    <div class="extra content">
		      <div class="ui two buttons">
		        <div class="ui basic green button">Modificar</div>
		        <div class="ui basic red button">Eliminar</div>
		      </div>
		    </div>
		  </div>

		</div>
	     
  	 </div> --}}

	  <div class="ui segment">
	  
	  	 <div class="ui center aligned grid">
		    
		     <div class="one column row"><p>¡Encuesta enviada con éxito! ¡Gracias!</p></div>
		   	 <div class="one column row"><p>¿Posee otra aplicación? Haga click </p></div>
		     <div class="one column row"><button @click="recargarPagina" class="ui button blue">Cargar nueva aplicación</button></div>
		    
		 </div>


	  </div>

</div>

</template>


<template v-if="habilitar_formulario">

	
	<div class="ui teal progress" id="progress_bar">
	  
	  <div class="bar">
	    <div class="progress"></div>
	  </div>

	  <div class="label">Porcentaje de la encuesta completada</div>

	</div>

 	
  <div class="ui styled fluid accordion">


	  <div class="title active" >

	  	<span v-bind:class="{'font-error': errores_form_software }">

	    <i  class="dropdown icon"></i>
	    Información del Software
	    <i v-show="save_form_software" class="fa fa-check" style="float: right;" aria-hidden="true"></i>
		<i v-show="errores_form_software"  style="float: right;" class="fa fa-times" aria-hidden="true"></i>

	    </span>
	  </div>
	  <div class="content active">

	    <div class="transition visible" >

			<br>

			<div class="ui form">

					<div class="ui grid">

  						<div class="one column row">
						    <div class="centered floated column">

						    	<div class="field" v-bind:class="{ 'disabled': save_form_software, 'error': errors.has('input_nombre_aplicacion') || errores.input_nombre_aplicacion }">
									<label>Nombre de la aplicación :</label>
									
									<input v-validate="'required'" v-on:keyup="limpiarNombreAplicacion" type="text" placeholder="" name="input_nombre_aplicacion" v-model="input_nombre_aplicacion" />
									
									
									<label class="error" v-show="errores.input_nombre_aplicacion">@{{errores.input_nombre_aplicacion}}</label>


									

								</div>

						    </div>
					  	</div>

  						<div class="one column row">
						    <div class="centered floated column">

						    	<div class="field" v-bind:class="{ 'disabled': save_form_software, 'error': errors.has('input_version_software') || errores.input_version_software }">
									<label>¿Versión actual de la aplicación?</label>
									<input v-validate="'required'" v-on:keyup="limpiarVersionSoftware" type="text" placeholder="" name="input_version_software" v-model="input_version_software" />
									<label class="error" v-show="errores.input_version_software">@{{errores.input_version_software}}</label>
									<label class="error" v-show="errors.has('input_version_software')">@{{errors.first('input_version_software')}}</label>
								</div>

						    </div>
					  	</div>


					  <div class="one column row">
					    <div class="centered floated column">

					    	<div class="field" v-bind:class="{ 'disabled': save_form_software, 'error': errors.has('input_descripcion_software') || errores.input_descripcion_software }">
								
								<label>Breve descripción / ¿Qué es lo que hace?</label>
								<textarea v-validate="'required'" v-on:keyup="limpiarDescripcion" placeholder="Descripción :" name="input_descripcion_software" v-model="input_descripcion_software"></textarea>
								
								<label class="error" v-show="errores.input_descripcion_software">@{{errores.input_descripcion_software}}</label>
								<label class="error" v-show="errors.has('input_descripcion_software')">@{{errors.first('input_descripcion_software')}}</label>

							</div>

					    </div>
					  </div>



					  <div class="two column row">

					    <div class="left floated column">

					    	{{-- <div class="field" v-bind:class="{ 'error': errors.has('input_lenguaje_programacion'), 'disabled': save_form_software }">

						      <label>Lenguaje de Programación </label>
						      <input  v-model="input_lenguaje_programacion" name="input_lenguaje_programacion" v-validate="'required'" placeholder="Lenguaje de programación:" type="text">

						      <label v-show="errors.has('input_lenguaje_programacion')" class="error"> @{{ errors.first('input_lenguaje_programacion')}}</label>

						    </div> --}}

 									  <template v-if="!otro_lenguaje">

								    	<div class="field" v-bind:class="{'disabled' : save_form_software, 'error': errors.has('input_lenguaje_programacion') || errores.lenguaje_nom }">
									      
									      <label>Lenguaje de Programación</label>

									      <select v-validate="'required'" v-model="input_lenguaje_programacion" name="input_lenguaje_programacion">
									      	
									      	<option value="" selected=""></option>
									      	<option v-for="lenguaje in all_lenguajes"  :value="lenguaje"> @{{ lenguaje }}</option>

									      </select>
										
										  <label class="error" v-show="errores.lenguaje_nom"> @{{errores.lenguaje_nom}} </label>
										  <label class="error" v-show="errors.has('input_lenguaje_programacion')">@{{errors.first('input_lenguaje_programacion')}}</label>

									    </div>

									  </template>

									  <template v-else>
									  	
									  	<div class="field" v-bind:class="{'disabled' : save_form_software}">
									      
									      <label>Lenguaje de Programación</label>

									  {{--   <v-select v-model="input_tipo_licencia" :options="all_licencias">
									    </v-select> --}}


									      <select  v-validate="'required'" v-model="input_lenguaje_programacion" name="input_lenguaje_programacion">
									      	
									      	<option value="" selected=""></option>
									      	<option v-for="lenguaje in all_lenguajes" > @{{ lenguaje }}</option>

									      </select>
									     

									    </div>

									  	<div class="field" v-bind:class="{'disabled' : save_form_software , 'error': errors.has('input_lenguaje_programacion') || errores.lenguaje_nom }">

									   	 <label>Ingrese el lenguaje</label>

									     <input @keyup="limpiarLenguajeNombre" v-validate="'required'" type="text" v-model="input_lenguaje_programacion_otro" placeholder=""  />


									      <label class="error" v-show="errores.lenguaje_nom"> @{{errores.lenguaje_nom}} </label>
										  <label class="error" v-show="errors.has('input_lenguaje_programacion')">@{{errors.first('input_lenguaje_programacion')}}</label>

									    </div>

									  </template>


					    </div>

					    <div class="right floated column">

					    	<div class="field" v-bind:class="{ 'error': errors.has('input_lenguaje_programacion_version') || errores.lenguaje_ver, 'disabled': save_form_software }">

						      <label>Versión del lenguaje</label>

						      <input  @keyup="limpiarLenguajeVersion" v-model="input_lenguaje_programacion_version" name="input_lenguaje_programacion_version" v-validate="'required'" placeholder="Versión:" type="text">
									      
							  <label class="error" v-show="errores.lenguaje_ver"> @{{errores.lenguaje_ver}} </label>
						      <label v-show="errors.has('input_lenguaje_programacion_version')" class="error"> @{{ errors.first('input_lenguaje_programacion_version')}}</label>

						    </div>

					    </div>

					  </div>

					  
					  <div class="two column row">

					    <div class="left floated column">

					    	<div class="field" v-bind:class="{ 'error': errors.has('version_mobile'), 'disabled': save_form_software }">

								<div class="ui slider checkbox" style="text-align:center; ">
								  <input type="checkbox" v-model="input_version_mobile" name="newsletter">
								  <label>¿Tiene versión mobile?</label>
								</div>

						   </div>

					    </div>

					    <div class="right floated column">

					    	<div class="field" v-bind:class="{ 'error': errors.has('version_web'), 'disabled': save_form_software }">

						     	<div class="ui slider checkbox">

								  <input type="checkbox" v-model="input_version_web" name="newsletter">
								  <label>¿Posee versión web?</label>

								</div>
						    </div>

					    </div>

					  </div>


					  <div class="two column row">

					    <div class="left floated column">


						     		 <template v-if="!otra_db">

								    	<div class="field" v-bind:class="{'disabled' : save_form_software, 'error': errors.has('input_base_dato')}">
									      <label>Base de datos asociada</label>

									      <select v-validate="'required'" v-model="input_base_dato" name="input_base_dato">
									      	<option value="" selected=""></option>
									      	<option v-for="db in all_dbs"  :value="db"> @{{ db }}</option>

									      </select>


									    </div>

									  </template>

									  <template v-else>
									  	
									  	<div class="field" v-bind:class="{'disabled' : save_form_software, 'error': errors.has('input_base_dato')}">
									      
									      <label>Base de datos asociada</label>

									      <select v-validate="'required'" v-model="input_base_dato" name="input_base_dato" >
									      	
									      	<option value="" selected=""></option>
									      	<option v-for="db in all_dbs" > @{{ db }}</option>

									      </select>

									    </div>

									  	<div class="field" v-bind:class="{'disabled' : save_form_software, 'error': errores.base_nom || errors.has('input_base_dato_otra') }">

									   	 <label>Ingrese la base de datos</label>

									     <input @keyup="limpiarBaseDatosNombre" v-validate="'required'" type="text" v-model="input_base_dato_otra" name="input_base_dato_otra" placeholder=""  />

							 			 <label class="error" v-show="errores.base_nom"> @{{errores.base_nom}} </label>
						      			 <label v-show="errors.has('input_base_dato_otra')" class="error"> @{{ errors.first('input_base_dato_otra')}}</label>

									    </div>

									  </template>

					    </div>

					    <div class="right floated column">

					    	<div class="field" v-bind:class="{ 'error': errors.has('input_base_dato_version'), 'disabled': save_form_software }">

						      <label>Versión de base de dato</label>

						      <input  v-model="input_base_dato_version" name="input_base_dato_version" v-validate="'required'" placeholder="Versión (BD):" type="text">

						      <label v-show="errors.has('input_base_dato_version')" class="error"> @{{ errors.first('input_base_dato_version')}}</label>

						    </div>

					    </div>

					  </div>

				


					   <div class="one column row">
					    <div class="left floated column">

					    	<template v-if="!save_form_software">

						    	<div class="field" style="text-align: center;">
									<div class="ui buttons">

										  <button class="ui button">Cancelar</button>
										  <div class="or"></div>
										  <button @click="onSaveFormSoftware" class="ui blue button active">Guardar</button>


									</div>

								</div>

							</template>

							<template v-else >

								<div style="text-align: center;">

									<button @click="onEditFormSoftware" class="orange ui button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar los datos</button>

									<button class="positive ui button"><i class="fa fa-check-square-o f-2x"></i>  Guardado los datos</button>

								</div>

							</template>


					    </div>
					  </div>


				</div>

			</div>
		</div>
	  </div>


  	 <div class="title">


	  	<span v-bind:class="{'font-error': _errores_form_instalacion }">

	  	<i  class="dropdown icon"></i>
	    Requerimientos de la instalación
	   <i v-show="save_form_instalacion" class="fa fa-check" style="float: right;" aria-hidden="true"></i>
	   <i v-show="_errores_form_instalacion"  style="float: right;" class="fa fa-times" aria-hidden="true"></i>

	   </span>

	  </div>
	  <div class="content" >
		  <div class="transition" >

		  <br>

		  		<div class="ui form">

						<div class="ui grid">

							  <div class="one column row">
							    
							    <div class="centered floated column">

							    	<div class="field" v-bind:class="{ 'disabled': save_form_instalacion, 'error' : errors.has('input_pasos_instalacion') || errores.input_pasos_instalacion  }">
										
										<label>Indique los pasos para su instalación:</label>

										<textarea @keyup="limpiarPasosInstalacion" v-model="input_pasos_instalacion" placeholder="" name="input_pasos_instalacion" ></textarea>

						      			<label v-show="errors.has('input_pasos_instalacion')" class="error"> @{{ errors.first('input_pasos_instalacion')}}</label>
							 			<label class="error" v-show="errores.input_pasos_instalacion"> @{{errores.input_pasos_instalacion}} </label>

									</div>

							    </div>
							  
							  </div>


							  <div class="one column row">
								    
								    <div class="left floated column">

								   	<template v-if="!save_form_instalacion">

								    	<div class="field" style="text-align: center;">
											<div class="ui buttons">
												  <button class="ui button">Cancelar</button>
												  <div class="or"></div>
												  <button @click="onSaveFormInstalacion" class="ui blue button active">Guardar</button>
												</div>
										</div>

									</template>

									<template v-else>

										<div style="text-align: center;">

											<button @click="onEditFormInstalacion" class="orange ui button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar los datos</button>

											<button class="positive ui button"><i class="fa fa-check-square-o f-2x"></i>  Guardado los datos</button>

										</div>

									</template>

								    </div>

						  		</div>

							  </div>
				</div>
		  </div>

		</div>


	  <div class="title">
	    
	   <span v-bind:class="{'font-error': errores_form_codigo_fuente }">

		    <i  class="dropdown icon"></i>
		    Información del código fuente
		    <i v-show="save_form_codigo_fuente" class="fa fa-check" style="float: right;" aria-hidden="true"></i>
		    <i v-show="errores_form_codigo_fuente"  style="float: right;" class="fa fa-times" aria-hidden="true"></i>

	   </span>

	  </div>	

	  <div class="content" >
	    <div class="transition" >

			<br>

	    	<div class="ui  form">


					<div class="ui grid">


					 		 <div class="two column row">
							    
							    <div class="left floated column" style="margin-top: 0.5%;">

							    	<div class="field" v-bind:class="{ 'error': errors.has('input_posee_repositorio'), 'disabled': save_form_codigo_fuente }">

								     			   
											<div class="ui slider checkbox">

											  <input type="checkbox" v-model="input_desarrollo_empresa" >
											   <label>¿La aplicación fue desarrollada por una empresa?</label>

											</div>

									</div>
								
								</div>


								<div class="right floated column">

									<template v-if="input_desarrollo_empresa">
										 
										 	<div class="field" v-bind:class="{ 'error': errors.has('input_desarrollo_empresa') || errores.input_nombre_empresa, 'disabled': save_form_codigo_fuente }">

										    	<div v-bind:class="{'disabled' : save_form_codigo_fuente}">

											      <input @keyup="limpiarNombreEmpresa" placeholder="Indique la empresa :" type="text" v-model="input_nombre_empresa">
											   		
											   	 <label class="font-error" v-if="errores.input_nombre_empresa">@{{this.errores.input_nombre_empresa}}</label>

											    </div>

											</div>


									</template>
								  		
	   
								  </div>

							 </div>


					 		 <div class="two column row">
							    
							    <div class="left floated column" style="margin-top: 0.5%;">

							    	<div class="field" v-bind:class="{ 'error': errors.has('input_posee_repositorio'), 'disabled': save_form_codigo_fuente }">
								     			   
											<div class="ui slider checkbox">

											  <input type="checkbox" v-model="input_desarrollo_codigo_fuente_disponible" >
											  <label>¿Posee el código fuente de la aplicación?</label>

											</div>

									</div>								
								</div>

							 </div>

							  <div class="one column row">
							    
							    <div class="centered floated column">

						    	<div class="field" v-bind:class="{ 'error': errors.has('input_posee_repositorio'), 'disabled': save_form_codigo_fuente }">

							     	<div class="ui slider checkbox">

									  <input type="checkbox" v-model="input_posee_repositorio" >
									   <label>¿Posee algun repositorio remoto del código fuente ? ( GitHub / Bitbucket ) </label>

									</div>

							    	<div v-show="input_posee_repositorio" class="field" v-bind:class="{'disabled' : save_form_codigo_fuente , 'error': errores.input_repositorio_remoto}">

								      <input @keyup="limpiarRepositorioRemoto" v-model="input_repositorio_remoto" style="margin-top: 15px;" placeholder="Link : (Ejemplo : https://github.com/facebook/react) " type="text" />
								   
								   	  <label class="font-error" v-if="errores.input_repositorio_remoto">@{{this.errores.input_repositorio_remoto}}</label>

								    </div>

								 </div>
   
							    </div>
							  </div>

							  <div class="one column row">
							    <div class="left floated column">

							    	<template v-if="!save_form_codigo_fuente">
								    	
								    	<div class="field" style="text-align: center;">
											<div class="ui buttons">
												  <button class="ui button">Cancelar</button>
												  <div class="or"></div>
												  <button @click="onSaveFormCodigoFuente" class="ui blue button active">Guardar</button>
												</div>
										</div>

							    	</template>


							    	<template v-else >

										<div style="text-align: center;">

											<button @click="onEditFormCodigoFuente" class="orange ui button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar los datos</button>

											<button class="positive ui button"><i class="fa fa-check-square-o f-2x"></i>  Guardado los datos</button>

										</div>

									</template>

							    </div>
					  		</div>

					</div>

			</div>
	    </div>
	  </div>

	  <div class="title">

	  	<span v-bind:class="{'font-error': errores_form_reparticion }">
	  	
	  	<i  class="dropdown icon"></i>
	    Información de la repartición
	    <i v-show="save_form_reparticion" class="fa fa-check" style="float: right;" aria-hidden="true"></i> 	
		<i v-show="errores_form_reparticion"  style="float: right;" class="fa fa-times" aria-hidden="true"></i>

	  	</span>
	  
	  </div>

	  <div class="content" >
	    <div class="transition" >

			<br>

	    	<div class="ui  form">

					<div class="ui grid">

							  <div class="one column row">
							    <div class="centered floated column">

							    	<div class="field" v-bind:class="{'disabled' : save_form_reparticion, 'error': errores.input_reparticion}" id="reparticion">
								      
								      <label>Repartición donde se encuentra instalado</label>
								      <input @keyup="limpiarReparticion" name="input_reparticion" v-model="input_reparticion" placeholder="Ej: Reparticion1 ; Reparticion2 ; Reparticion3 (separados por punto y coma en el caso de ser más de una). " type="text">
								    
							 		  <label class="error" v-show="errores.input_reparticion"> @{{errores.input_reparticion}} </label>
							 		  <label class="error" v-show="errors.has('input_reparticion')"> @{{ errors.first('input_reparticion') }} </label>
							 		  
							 		  <div class="ui special popup" id="popup_reparticion">
  										<div>Se refiere a otras áreas u organismos que utilicen esta aplicación.</div>
									  </div>

								    </div>


							    </div>
							  </div>

							  <div class="one column row">
							    <div class="left floated column">

							    	<template v-if="!save_form_reparticion">
								    	<div class="field" style="text-align: center;">
											<div class="ui buttons">
												  <button class="ui button">Cancelar</button>
												  <div class="or"></div>
												  <button @click="onSaveFormReparticion" class="ui blue button active">Guardar</button>
												</div>
										</div>

							    	</template>


							    	<template v-else >

										<div style="text-align: center;">

											<button @click="onEditFormReparticion" class="orange ui button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar los datos</button>

											<button class="positive ui button"><i class="fa fa-check-square-o f-2x"></i>  Guardado los datos</button>

										</div>

									</template>

							    </div>
					  		</div>

					</div>

			</div>
	    </div>
	  </div>



	  <div class="title">
	    
	    <i  class="dropdown icon"></i>
	    Información del responsable
	    <i v-show="save_form_responsable" class="fa fa-check" style="float: right;" aria-hidden="true"></i>

	  </div>

	  <div class="content" >

		  <div class="transition" >

		  <br>

		  		<div class="ui form">

						<div class="ui grid">

								  
							<template v-if="!agregar_responsable">
								

 									<div v-for="responsable in all_responsables" class="six column row">

								  	    
 										<template v-if="!responsable.editar_responsable">


									  	    <div class="left floated column">

										    	<div class="field disabled" v-bind:class="{'disabled' : save_form_responsable, 'error' : errores.nombre}">
											      <label>Responsable del Software</label>
											      <input placeholder="" type="text" v-model="responsable.nombre">
											      <label v-show="errores.nombre">@{{errores.nombre}}</label>
											    </div>

										    </div>

										    <div class="left floated column">

										    	<div class="field disabled" v-bind:class="{'disabled' : save_form_responsable, 'error' : errores.correo}">
											      <label> Correo  </label>
											      <input placeholder="" type="text" v-model="responsable.correo">
											    </div>

										    </div>							    

										    <div class="left floated column">

										    	<div class="field disabled" v-bind:class="{'disabled' : save_form_responsable}">
											      <label> Teléfono (celular) </label>
											      <input placeholder="" type="text" v-model="responsable.telefono_celular">
											    </div>

										    </div>

		    								<div class="left floated column">

										    	<div class="field disabled" v-bind:class="{'disabled' : save_form_responsable}">
											      <label> Teléfono (interno) </label>
											      <input placeholder="" type="text" v-model="responsable.telefono_interno">
											    </div>

										    </div>


											 <div class="right floated column">

											   <div class="field" v-bind:class="{'disabled' : save_form_responsable}">
												     
												   <button style="margin-top: 16%;" @click="onEditarResponsable(responsable)"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>											    
												   
											   </div>

											 </div>

										</template>

										
										<template v-else> {{-- habilitado para editar --}}
								  	   	
									  	   	
									  	 <div class="left floated column">
											<div class="field " v-bind:class="{'disabled' : save_form_responsable, 'error' : errores.nombre}">
										      <label>Responsable del Software</label>
										      <input placeholder="" type="text" v-model="responsable.nombre">
										      <label v-show="errores.nombre">@{{errores.nombre}}</label>
										    </div>

									    </div>

									    <div class="left floated column">

									    	<div class="field " v-bind:class="{'disabled' : save_form_responsable}">
										      <label> Correo </label>
										      <input placeholder="" type="text" v-model="responsable.correo">
										    </div>

									    </div>							    

									    <div class="left floated column">

									    	<div class="field " v-bind:class="{'disabled' : save_form_responsable}">
										      <label> Teléfono (celular) </label>
										      <input placeholder="" type="text" v-model="responsable.telefono_celular">
										    </div>

									    </div>

	    								<div class="left floated column">

									    	<div class="field " v-bind:class="{'disabled' : save_form_responsable}">
										      <label> Teléfono (interno) </label>
										      <input placeholder="" type="text" v-model="responsable.telefono_interno">
										    </div>

									    </div>
										    <div class="left floated column">

										    	<div class="field" v-bind:class="{'disabled' : save_form_responsable}">
											     
											     <button style="margin-top: 16%; margin-left:5%;" @click="onGuardarCambios(responsable)">Guardar cambios</button>
											    
											    </div>

										    </div>

										    <div class="right floated column">

										    	<div class="field" v-bind:class="{'disabled' : save_form_responsable}">
											     
											     <button style="margin-top: 16%;" @click="onEditarResponsable(responsable)"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
											    
											    </div>

										    </div>


								  	 </template>



								  	 </div>
								  

									<div class="five column row">
									  
									  <div class="centered floated column">
									    <div class="field" v-bind:class="{'disabled' : save_form_responsable}">
								

										   		<span  style="margin-left: 10%;" @click="setAgregarResponsable" ><i class="fa fa-plus-circle" aria-hidden="true"></i><label style="margin-left:5px;font-size: 15px;">agregar responsable</label></span>
										   	

									    </div>
									   </div>
								    
								    </div>

							</template>


							<template v-else>


								  	<div v-for="responsable in all_responsables" class="six column row">

								  	   
								  	   <template v-if="!responsable.editar_responsable">
								  	   	
									  	   	
									  	<div class="left floated column">
											<div class="field disabled" v-bind:class="{'disabled' : save_form_responsable}">
										      <label>Responsable del Software</label>
										      <input placeholder="" type="text" v-model="responsable.nombre">
										    </div>

									    </div>

									    <div class="left floated column">

									    	<div class="field disabled" v-bind:class="{'disabled' : save_form_responsable}">
										      <label> Correo </label>
										      <input placeholder="" type="text" v-model="responsable.correo">
										    </div>

									    </div>							    

									    <div class="left floated column">

									    	<div class="field disabled" v-bind:class="{'disabled' : save_form_responsable}">
										      <label> Teléfono (celular) </label>
										      <input placeholder="" type="text" v-model="responsable.telefono_celular">
										    </div>

									    </div>

	    								<div class="left floated column">

									    	<div class="field disabled" v-bind:class="{'disabled' : save_form_responsable}">
										      <label> Teléfono (interno) </label>
										      <input placeholder="" type="text" v-model="responsable.telefono_interno">
										    </div>

									    </div>

										    <div class="left floated column">


										    
										    </div>

										    <div class="right floated column">

										    	<div class="field" v-bind:class="{'disabled' : save_form_responsable}">
											     
											      <button style="margin-top: 16%;" @click="onEditarResponsable(responsable)"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
											    
											    </div>

										    </div>


								  	   </template>


								  	   <template v-else> {{-- habilitado para editar --}}
								  	   	
									  	   	
									  	 <div class="left floated column">
											<div class="field " v-bind:class="{'disabled' : save_form_responsable}">
										      <label>Responsable del Software</label>
										      <input placeholder="" type="text" v-model="responsable.nombre">
										    </div>

									    </div>

									    <div class="left floated column">

									    	<div class="field " v-bind:class="{'disabled' : save_form_responsable}">
										      <label> Correo </label>
										      <input placeholder="" type="text" v-model="responsable.correo">
										    </div>

									    </div>							    

									    <div class="left floated column">

									    	<div class="field " v-bind:class="{'disabled' : save_form_responsable}">
										      <label> Teléfono (celular) </label>
										      <input placeholder="" type="text" v-model="responsable.telefono_celular">
										    </div>

									    </div>

	    								<div class="left floated column">

									    	<div class="field " v-bind:class="{'disabled' : save_form_responsable}">
										      <label> Teléfono (interno) </label>
										      <input placeholder="" type="text" v-model="responsable.telefono_interno">
										    </div>

									    </div>
										    <div class="left floated column">

										    	<div class="field" v-bind:class="{'disabled' : save_form_responsable}">
											     
											     <button style="margin-top: 16%; margin-left:5%;" @click="onGuardarCambios(responsable)">Guardar cambios</button>
											    
											    </div>

										    </div>

										    <div class="right floated column">

										    	<div class="field" v-bind:class="{'disabled' : save_form_responsable}">
											     
											     <button style="margin-top: 16%;" @click="onEditarResponsable(responsable)"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
											    
											    </div>

										    </div>


								  	 </template>


								  	    
								 </div>
								  
								  	   
								  <div class="five column row">

								  	<div class="left floated column">
											<div class="field " v-bind:class="{'disabled' : save_form_responsable, 'error' : errores.nombre_responsable || errors.has('nombre_responsable') }">
										      <label>Responsable del Software</label>
										      <input v-validate="{ rules: { required: true } }" @keyup="limpiarNombreResponsable" placeholder="" type="text" v-model="input_responsable_nombre" name="nombre_responsable">
										      <label v-show="errores.nombre_responsable">@{{errores.nombre_responsable}}</label>
										      <label v-show="errors.has('nombre_responsable')">@{{this.errors.first('nombre_responsable')}}</label>
										    </div>

									    </div>

									    <div class="left floated column">

									    	<div class="field " v-bind:class="{'disabled' : save_form_responsable, 'error' : errores.correo || errors.has('responsable_correo')}">
										      <label> Correo : </label>
										      <input v-validate="{ rules: { email: true } }" @keyup="limpiarCorreoResponsable" placeholder="" type="text" v-model="input_responsable_correo" name="responsable_correo">
										 	  <label v-show="errores.correo">@{{errores.correo}}</label>
										 	  <label v-show="errors.has('responsable_correo')">@{{this.errors.first('responsable_correo')}}</label>
										    </div>

									    </div>							    

									    <div class="left floated column">

									    	<div class="field" v-bind:class="{'disabled' : save_form_responsable, 'error': errores.telefono_celular}">
										      <label> Teléfono (celular) : </label>
										      <input  @keyup="limpiarTelefonoCelularResponsable" placeholder="" type="email" v-model="input_responsable_telefono_celular">
										  	  <label v-show="errores.telefono_celular">@{{errores.telefono_celular}}</label>
										    </div>

									    </div>

	    								<div class="left floated column">

									    	<div class="field " v-bind:class="{'disabled' : save_form_responsable, 'error': errores.telefono_interno}">
										      <label> Teléfono (interno) : </label>
										      <input @keyup="limpiarTelefonoInternoResponsable" placeholder="" type="text" v-model="input_responsable_telefono_interno">
										      <label v-show="errores.telefono_interno">@{{errores.telefono_interno}}</label>

										    </div>

									    </div>

									    <div class="left floated column">

									    	<div class="field" v-bind:class="{'disabled' : save_form_responsable}">
										     
										     <button @click="onAgregarResponsable" style="margin-top: 16%;  margin-left:5%;">Agregar</button>
										    
										    </div>

									    </div>
								  	 </div>

						
						 </template>
									


								 <div class="one column row">
								   
								    <div class="left floated column">

								    	<template v-if="!save_form_responsable">

								    	<div class="field" style="text-align: center;">
											<div class="ui buttons">
												  <button class="ui button">Cancelar</button>
												  <div class="or"></div>
												  <button @click="onSaveFormResponsable" class="ui blue button active">Guardar</button>
												</div>
										</div>

										</template>
										<template v-else>

										<div style="text-align: center;">

											<button @click="onEditFormResponsable" class="orange ui button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar los datos</button>

											<button class="positive ui button"><i class="fa fa-check-square-o f-2x"></i>  Guardado los datos</button>

										</div>

										</template>

								    </div>
						  		</div>

							  </div>

				</div>
		  </div>

		</div>


	{{-- SECCION INFORMACION DE LICENCIA --}}

	  <div class="title">
	    <span v-bind:class="{'font-error': errores_form_licencia}">

		    <i  class="dropdown icon"></i>
		    Información de la licencia

		    <i v-show="save_form_licencia" class="fa fa-check" style="float: right;" aria-hidden="true"></i>
		    <i v-show="errores_form_licencia"  style="float: right;" class="fa fa-times" aria-hidden="true"></i>

	    </span>
	  </div>

	  <div class="content" >
		 
		  <div class="transition" >

		  		<div class="ui form">

						<div class="ui grid">

							<div class="one column row">
								    
								    <div class="centered floated column">

								    <template v-if="!otra_licencia">

								    	<div class="field" v-bind:class="{'disabled' : save_form_licencia, 'error': errores.input_tipo_licencia}">
									      
									      <label>Tipo de licencia :</label>

									  {{--   <v-select v-model="input_tipo_licencia" :options="all_licencias">
									    </v-select> --}}


									      <select v-validate="'required'" v-model="input_tipo_licencia" name="input_tipo_licencia">
									      	
									      	<option value="" selected=""></option>
									      	<option v-for="licencia in all_licencias" > @{{ licencia }}</option>

									      </select>
									      <label class="error" v-show="errores.input_tipo_licencia"> @{{errores.input_tipo_licencia}} </label>


									    </div>

									  </template>

									  <template v-else>
									  	
									  	<div class="field" v-bind:class="{'disabled' : save_form_licencia}">
									      
									      <label>Tipo de licencia :</label>

									      <select v-validate="'required'" v-model="input_tipo_licencia" name="input_tipo_licencia" >
									      	
									      	<option value="" selected=""></option>
									      	<option v-for="licencia in all_licencias" > @{{ licencia }}</option>

									      </select>


									    </div>

									 	 <div class="field" v-bind:class="{'disabled' : save_form_licencia, 'error' : errores.input_tipo_licencia}">

									   	 <label>Ingrese la licencia</label>

									     <input type="text" v-model="input_tipo_licencia_otra" name="input_tipo_licencia_otra" placeholder="Licencia ..."  />

							 		  	<label class="error" v-show="errores.input_tipo_licencia"> @{{errores.input_tipo_licencia}} </label>

									    </div>

									  </template>

								    </div>
								  </div>


								 <div class="one column row">
								    <div class="left floated column">

								    	<template v-if="!save_form_licencia">

								    	<div class="field" style="text-align: center;">
											<div class="ui buttons">
												  <button class="ui button">Cancelar</button>
												  <div class="or"></div>
												  <button @click="onSaveFormLicencia" class="ui blue button active">Guardar</button>
												</div>
										</div>

										</template>

										<template v-else>

										<div style="text-align: center;">

											<button @click="onEditFormLicencia" class="orange ui button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar los datos</button>

											<button class="positive ui button"><i class="fa fa-check-square-o f-2x"></i>  Guardado los datos</button>

										</div>

										</template>

								    </div>
						  		</div>
					
					 </div>
				</div>
		
		  </div>

		</div>


</div>
		

		<br>
		<br>

		<div style="text-alignn: center;" >
			<button @click="enviarFormulario" v-show="encuesta_completa" class="positive ui big button" id="boton_enviar">Enviar encuesta</button>
		</div>


</template>


{{-- UNA VEZ QUE SE ENVIA LA ENCUESTA, SE HABILITA EL SPINNER --}}

<template v-if="habilitar_loading">

		  <div class="ui active inverted dimmer">
		    <div class="ui text loader">Enviando datos...</div>
		  </div>

</template>




@endif


<div class="ui modal">

	  <div class="header">
	    Acceder al formulario
	  </div>


	  <div class="image content">
	    
	    <div class="image">
	      <img style="width: 200px;" src="http://www.freeiconspng.com/uploads/register-secure-security-user-login-icon--7.png">
	    </div>

		<div class="description">
			

				<div class="ui form" style="margin-top:5%; margin-left:10%; width:500px">
					<div class="field" v-bind:class="{'error' : errores.email}">

						<label>Ingrese el e-mail:</label> 
						<input type="text" placeholder="E-mail:" name="email" v-model="user_email">

						<label v-if="errores.email">@{{errores.email[0]}}</label>

					</div> 
					<div class="field" v-bind:class="{'error' : errores.password}">

						<label>Ingrese el password:</label> 
						<input type="password" placeholder="Password:" name="password" v-model="user_password">
						
						<label v-if="errores.password">@{{errores.password[0]}}</label>

					</div>
				</div>

		</div>

	  </div>


	  <div class="actions">
	    <div @click="closeModalOn" class="ui button">Cancelar</div>
	    <button @click="logearUsuario" type="submit" class="ui button">OK</button>
	  </div>



	</div>

</div>

</div>
	


@endsection


@section('scripts')

{{-- <script type="text/javascript" src="http://closure-compiler.appspot.com/code/jsc81247ddc5c50e240d900582629789a85/default.js" ></script>
 --}}

<script type="text/javascript">


  Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr('value');


  Vue.use(VeeValidate);
  Vue.component('v-select', VueSelect.VueSelect);


  var vm = new Vue({

    el: '#manage-vue',

    data: function(){

      return {

      	usuario_logeado_organismo_id : '{{ !Auth::guest() ?  \Auth::user()->organismo_id : ''}}' ,

		input_nombre_aplicacion: '',
      	input_descripcion_software: '',
	    input_version_software:'',
	    
	    input_lenguaje_programacion:'',
	    input_lenguaje_programacion_otro:'',
	    input_lenguaje_programacion_version:'',
	    
	    input_base_dato:'',
	    input_base_dato_otra:'',
	    input_base_dato_version:'',


	    input_version_mobile:'',
	    input_version_web:'',


	    input_pasos_instalacion:'',

    
	    input_tipo_licencia:'',
	    input_tipo_licencia_otra:'',

	    input_desarrollo_codigo_fuente_disponible: false,


	    input_posee_repositorio: false,
	   	input_repositorio_remoto: '',


	    input_desarrollo_empresa: false,
	   	input_nombre_empresa:'',

	    input_reparticion: '',

	    input_responsable_nombre: '',
	    input_responsable_correo: '',
	    input_responsable_telefono_celular: '',
	    input_responsable_telefono_interno: '',

	    user_email:'',
	    user_password:'',


	    //variables de la sección de ENCUESTAS GENERALES

	    encuesta_info_general: '', //encuesta que traemos de haberla ya cargado

	
	    	
	    //fin variables de seccion de ENCUESTAS GENERALES

	    errores:[],

	    errores_form_software : false,
		errores_form_instalacion: false,
		errores_form_reparticion: false,
		errores_form_licencia: false,
		errores_form_codigo_fuente: false,

	    all_responsables:[],

	    all_aplicaciones: [],

	    all_lenguajes: ['ABAP2','ABL3','ActionScript',
		'ActionScript 3','C Sharp (C#)','Clarion','Clipper','D',
		'Object Pascal (Embarcadero Delphi)','Gambas','GObject','Genie','Harbour',
		'Eiffel','Fortran 90/95','Java','JavaScript','Lexico','Objective-C',
		'Ocaml','Oz','R','Pascal','Perl','PHP','PowerBuilder','Processing','Python','Ruby','Self',
		'Smalltalk','Magik','Vala','VB.NET','Visual FoxPro','Visual Basic 6.0','Visual DataFlex',
		'Visual Objects','XBase++','DRP','Scala',
		'Otro ...'
		],

		all_dbs : ['DB2','Firebird','HSQL','Informix','Interbase','MariaDB','Microsoft SQL Server','MySQL','Oracle','PostgreSQL','PervasiveSQL','SQLite','Sybase ASE', 'Otra ...'],

	    all_licencias : [

		'Academic Free License','Apache License','Apple Public Source License','Artistic License','Berkeley Database License','BSD license',
		'Boost Software License','Common Development and Distribution','Common Public License','Creative Commons Licenses',
		'Cryptix General License','Eclipse Public License','Educational Community License','Eiffel Forum License',
		'GNU General Public License','GNU Lesser General Public License','Hacktivismo Enhanced-Source Software License Agreement',
		'IBM Public License','Intel Open Source License','ISC license Internet Systems','LaTeX Project Public License',
		'MIT license / X11 license','Mozilla Public License','Netscape Public License','Open Software License',
		'OpenSSL license','PHP License','Poetic License','Python Software','Q Public License','Sun Industry Standards Source License','Sun Public License',
		'Sybase Open Watcom','W3C Software Notice and License','XFree86 1.1 License','Licencia zlib/libpng','Zope Public License',
		'Otra ...'
	    ],


    	save_form_software:false,
    	save_form_reparticion:false,
    	save_form_responsable:false,
    	save_form_instalacion:false,
    	save_form_licencia:false,
    	save_form_codigo_fuente:false,

    	agregar_responsable:false,

    	habilitar_loading: false,
    	habilitar_ending_page: false,
    	habilitar_formulario: true,


        // path: window.location.href.slice(0,-4)
       
        path: window.location.href


      }

    },


    methods: {

      getLocalidades: function(){

      this.$http.get( this.path +'/ver_localidades' ).then((response) =>{

          this.$set(this,'all_localidades', response.data);

        }, (response) => {


        });

      },

     getAplicaciones: function(){

      this.$http.get( this.path +'/aplicaciones/usuario' ).then((response) =>{

          this.$set(this,'all_aplicaciones', response.data);

        }, (response) => {


        });

      },

      getInfoGeneral : function(){

      this.$http.post( this.path + '/informacion_general/get_by_organismo' ,
      					{

      					organismo_id : this.usuario_logeado_organismo_id
      					
      					}
      					).then ( (response) => {

      						this.$set(this, 'encuesta_info_general', response.data);


      					}, (response) => {

      						this.$set(this,'encuesta_info_general','');

      					});


      },




      onSaveFormSoftware: function(){

      	var hay_errores_nombre_aplicacion = this.errors.has('input_nombre_aplicacion'); 
      	if(this.input_version_software == '')  this.$set(this.errores,'input_nombre_aplicacion', 'Debe ingresar el nombre software.');


      	var hay_errores_version = this.errors.has('input_version_software') || (this.input_version_software == ''); 
      	if(this.input_version_software == '')  this.$set(this.errores,'input_version_software', 'Debe ingresar la versión del software.');

      	var hay_errores_descripcion= this.errors.has('input_descripcion_software') || (this.input_descripcion_software == '');
 		if(this.input_descripcion_software == '')  this.$set(this.errores,'input_descripcion_software', 'Debe ingresar una descripción de la aplicación.');


 		this.errores_form_software = (hay_errores_descripcion || hay_errores_version || hay_errores_nombre_aplicacion) ? true : false;


		if(!this.save_form_software && !hay_errores_nombre_aplicacion && !hay_errores_version && !hay_errores_descripcion)
		{
			$('#progress_bar').progress('increment');
			this.save_form_software = true;

		}else{

			toastr.error('Revisar los campos.','¡Hay errores!', {timeOut: 5000});

			this.show_form_software = false;

		}


      },


      onSaveFormResponsable: function(){


      	if(this.agregar_responsable) this.agregar_responsable = false;

		if(!this.save_form_responsable)
		{
			$('#progress_bar').progress('increment');
			this.save_form_responsable = true;
		}

		this.show_form_responsable = false;


      },


      onSaveFormReparticion: function(){

      	var hay_errores_reparticion =  (this.input_reparticion == ''); 
      	
      	if(this.input_reparticion == '')  this.$set(this.errores, 'input_reparticion' , 'Debe los nombres de las reparticiones.');

      	this.errores_form_reparticion = hay_errores_reparticion;
      	

		if(!this.save_form_reparticion && !hay_errores_reparticion)
		{
			$('#progress_bar').progress('increment');
			this.save_form_reparticion = true;

		}else{

		
		toastr.error('Revisar los campos de las reparticiones.','¡Hay errores!', {timeOut: 5000});

		this.show_form_reparticion = false;


		}



      },

      onSaveFormInstalacion: function(){

		var hay_errores_instalacion =  (this.input_pasos_instalacion == ''); 
      	
      	if(this.input_pasos_instalacion == '')  this.$set(this.errores,'input_pasos_instalacion', 'Debe ingresar los pasos para llevar a cabo su instalación.');

      	this.errores_form_instalacion = hay_errores_instalacion;

		if(!this.save_form_instalacion && !hay_errores_instalacion)
		{

			$('#progress_bar').progress('increment');
			this.$set(this,'save_form_instalacion', true);

		}else{
		
		 toastr.error('Revisar los campos.','¡Hay errores!', {timeOut: 5000});
		 this.$set(this,'save_form_instalacion', false);

		}


      },

      onSaveFormLicencia: function(){

      	if(this.input_tipo_licencia == '')  
      		this.$set(this.errores,'input_tipo_licencia', 'Debe seleccionar una licencia del software.');
      	else if( (this.input_tipo_licencia == 'Otra ...') && this.input_tipo_licencia_otra == '' )  
      		this.$set(this.errores,'input_tipo_licencia', 'Debe ingresar una licencia del software.');

		var hay_errores_licencia = this.errores.input_tipo_licencia; 

		this.errores_form_licencia = hay_errores_licencia;
		console.log(this.errores_form_licencia);

		if(!this.save_form_licencia && !hay_errores_licencia)
		{

			$('#progress_bar').progress('increment');
			this.$set(this,'save_form_licencia', true);

		}else{

		 toastr.error('Revisar los campos a la hora de seleccionar la licencia.','¡Hay errores!', {timeOut: 5000});

		 this.$set(this,'save_form_licencia', false);

		}


      },

      onSaveFormCodigoFuente: function(){

      	let hay_errores_nombre_empresa = false;
      	let hay_errores_repositorio_remoto = false;

      	if(this.input_desarrollo_empresa && (this.input_nombre_empresa == ''))
      	{
      		this.$set(this.errores,'input_nombre_empresa', 'Debe ingresar el nombre de la empresa la cual se encargo del desarrollo de la app.');
      		hay_errores_nombre_empresa = true;
      	
      	}

      	if(this.input_posee_repositorio && (this.input_repositorio_remoto == ''))
      	{
      		this.$set(this.errores,'input_repositorio_remoto', 'Debe ingresar el nombre del repositorio.');
      		hay_errores_repositorio_remoto = true;
      	
      	}

		if(!this.save_form_codigo_fuente && !hay_errores_nombre_empresa && !hay_errores_repositorio_remoto)
		{
			this.$set(this,'errores_form_codigo_fuente', false);

			$('#progress_bar').progress('increment');
			this.$set(this,'save_form_codigo_fuente', true);

		}
		else{
			
		 this.$set(this,'errores_form_codigo_fuente', true);

		 this.$set(this,'save_form_codigo_fuente', false);
		 toastr.error('Revisar la información del campo.','¡Hay errores!',{timeOut: 5000});

		}

      },


      //al presionar el boton de guardar en el formulario de encuestas generales


      onSaveFormInformacionGeneral: function(){

      	if( this.informacion_general.aplicacion_ofimatica == '')
      	{
      		this.informacion_general.error_aplicacion_ofimatica = 'Debe seleccionar una aplicacion de ofimática.';

      	}
      	else
      	{
      		if(this.informacion_general.otra_aplicacion_ofimatica)
      		{
      			this.informacion_general.error_aplicacion_ofimatica = (this.informacion_general.otra_aplicacion_ofimatica_input == '') ? 'Debe ingresar una aplicacion de ofimática' : '';
      		}
      	}

      	
      	this.informacion_general.error_sistema_operativo = (this.informacion_general.sistema_operativo == '') ? 'Debe seleccionar alguna sistema operativo de uso': false;


      	if(!this.informacion_general.error_aplicacion_ofimatica && !this.informacion_general.error_sistema_operativo){

			$('#progress_bar').progress('increment');

			this.informacion_general.save_form = true;

			this.informacion_general.hay_errores = false;

			//this.enviarInformacionGeneral();
      	}
      	else{

		 this.informacion_general.save_form = false;

		 this.informacion_general.hay_errores = true;

		 toastr.error('Revisar los campos.','¡Hay errores!',{timeOut: 5000});


      	}
      
      },


      onEditFormSoftware: function(){


		$('#progress_bar').progress('decrement');
		this.save_form_software = false;


      },

      onEditFormCodigoFuente: function(){


		$('#progress_bar').progress('decrement');
		this.save_form_codigo_fuente = false;


      },

      onEditFormReparticion: function(){


		$('#progress_bar').progress('decrement');
		this.save_form_reparticion = false;


      },

      onEditFormResponsable: function(){


		$('#progress_bar').progress('decrement');
		this.save_form_responsable = false;


      },

      onEditFormInstalacion: function(){

		$('#progress_bar').progress('decrement');
		this.save_form_instalacion = false;

      },

      onEditFormLicencia: function(){

		$('#progress_bar').progress('decrement');
		this.save_form_licencia = false;

      },


      onEditFormInformacionGeneral: function(){

		$('#progress_bar').progress('decrement');
		this.informacion_general.save_form = false;

      },


      setAgregarResponsable: function(){

      	this.agregar_responsable = true;

      },

      onAgregarResponsable: function(){
      	
      	var hay_errores = false;
      	
      	if(this.input_responsable_nombre == '')
      	{	
      		this.$set(this.errores,'nombre_responsable', 'Debe insertar el nombre del responsable');
      		hay_errores = true;
      	}      	
      	if(this.input_responsable_correo == '')
      	{      		
      		this.$set(this.errores,'correo', 'Debe insertar el correo del responsable');
      		hay_errores = true;
      	}

      	
      	if(!hay_errores && !this.errors.has('responsable_correo') && !this.errors.has('nombre_responsable'))
      	{	

    		this.all_responsables.push({
      								nombre: this.input_responsable_nombre,
      								correo: this.input_responsable_correo,
      								telefono_celular: this.input_responsable_telefono_celular,
      								telefono_interno: this.input_responsable_telefono_interno,
      								editar_responsable: false
      							   });

      		this.$set(this,'agregar_responsable',false);      		
      		this.editar_responsable = false;

      		this.$set(this,'input_responsable_nombre','');
      		this.$set(this,'input_responsable_correo','');
      		this.$set(this,'input_responsable_telefono_celular','');
      		this.$set(this,'input_responsable_telefono_interno','');

      	}


      },

      onGuardarCambios: function(responsable){

      	Vue.set(responsable,'editar_responsable',false);

      },


      onEditarResponsable: function(responsable){

      	Vue.set(responsable,'editar_responsable',true);


      },


      setModalOn:function (){


      	$('.ui.modal').modal('show');

      },

       closeModalOn:function (){


      	$('.ui.modal').modal('hide');

      },

      logearUsuario:function(){


      this.$http.post( this.path +'auth/login', {email: this.user_email , password: this.user_password} ).then((response) =>{


          $('.ui.modal').modal('hide');

          //location.reload();


        }, (response) => {

        	this.$set(this,'errores',response.data);

        });      

  	  },

  	  enviarFormulario: function(){

  	  	
  	  	var lenguaje_nombre = this.input_lenguaje_programacion == 'Otro ...' ? this.input_lenguaje_programacion_otro : this.input_lenguaje_programacion;
  	  	var base_dato_nombre = this.input_base_dato == 'Otra ...' ? this.input_base_dato_otra : this.input_base_dato;
  	  	

  	  	var version_mob = this.input_version_mobile ? 1 : 0;
  	  	var version_web = this.input_version_web    ? 1 : 0;

  	  	var empresa_nom = this.input_desarrollo_empresa ? this.input_nombre_empresa : '*';
  	  	
  	  	var repositorio_remoto = this.input_posee_repositorio ? this.input_repositorio_remoto : '*';

  	  	var posee_codigo_fuente = this.input_desarrollo_codigo_fuente_disponible ? 'si' : 'no' ;


  	  	var tipo_licencia = this.input_tipo_licencia == 'Otra ...' ? this.input_tipo_licencia_otra : this.input_tipo_licencia;



  	  	this.$http.post( this.path + 'aplicacion/store', 

  	  					   {
  	  							nombre_aplicacion : this.input_nombre_aplicacion,
  	  							version_sof : this.input_version_software,
  	  							descripcion : this.input_descripcion_software,
  	  							lenguaje_nom : lenguaje_nombre,
  	  							lenguaje_ver : this.input_lenguaje_programacion_version,
  	  							version_mob : version_mob,
  	  							version_web : version_web,
  	  							base_nom  :base_dato_nombre,
  	  							base_ver : this.input_base_dato_version,

  	  							instalacion: this.input_pasos_instalacion,

  	  							desa_por_emp: this.input_desarrollo_empresa,
  	  							empresa_nom: empresa_nom,
  	  							cod_fuente: posee_codigo_fuente,
  	  							repositorio: repositorio_remoto,

  	  							reparticion: this.input_reparticion,

  	  							responsables : this.all_responsables,

  	  							licencia: tipo_licencia


  	  						}).then((response) => {


  	  						this.getAplicaciones();

  	  						var that = this;

						  	  
						  	 this.$set(this,'habilitar_loading', true);

						  	 this.$set(this,'habilitar_formulario', false);


						  	  setTimeout(function(){

						  	  		that.$set(that,'habilitar_loading', false);
						  	  		that.$set(that,'habilitar_ending_page', true);


						  	  	},3000);

				

  	  						}, (response) => {

          						
						  	  	this.$set(this,'errores', response.data);


  	  						});


  	  },

  	  enviarInformacionGeneral: function(){


  	  let sistema_operativo = this.informacion_general.otro_sistema_operativo ? this.informacion_general.otro_sistema_operativo_input : this.informacion_general.sistema_operativo;
  	  
  	  let aplicacion_ofimatica = this.informacion_general.otra_aplicacion_ofimatica ? this.informacion_general.otra_aplicacion_ofimatica_input : this.informacion_general.aplicacion_ofimatica;

  	  if(this.informacion_general.otra_aplicacion_ofimatica_input == '') this.informacion_general.error_aplicacion_ofimatica = ''

  	  this.$http.post( this.path + '/informacion_general/store',
  	  				{

  	  				sistema_operativo,
  	  				aplicacion_ofimatica,


  	  				}).then( (response) => {



  	  }, (response) => {


  	  }
  	  );


  	  },


  	  limpiarNombreAplicacion: function(){

  	  	this.errores.input_nombre_aplicacion = "";

  	  }, 	  


  	  limpiarVersionSoftware: function(){

  	  	this.errores.input_version_software = "";

  	  }, 	  

  	  limpiarDescripcion: function(){

  	  	this.errores.input_descripcion_software = "";

  	  },  	

  	  limpiarLenguajeNombre: function(){

  	  	this.errores.lenguaje_nom = "";

  	  }, 

  	  limpiarLenguajeVersion: function(){

  	  	this.errores.lenguaje_ver = "";

  	  },  	

  	  limpiarBaseDatosVersion: function(){

  	  	this.errores.base_ver = "";

  	  },

  	  limpiarBaseDatosNombre: function(){

  	  	this.errores.base_nom = "";

  	  },

  	  limpiarPasosInstalacion: function(){

  	  	this.errores.input_pasos_instalacion = "";
  	  	this.errores_form_instalacion = false;

  	  }, 

  	  limpiarReparticion: function(){

  	    this.errores.input_reparticion = ""; 	
  	  	this.errores_form_reparticion = false;

  	  },

  	  limpiarNombreResponsable: function(){

  	  	this.errores.nombre_responsable = "";

  	  },

  	  limpiarCorreoResponsable: function(){

  	  	this.errores.correo = "";

  	  },

  	  limpiarTelefonoCelularResponsable: function(){

  	  	this.errores.telefono_celular = "";

  	  },

  	  limpiarTelefonoInternoResponsable: function(){

  	  	this.errores.telefono_interno = "";

  	  },


	//limpiar errores en el campo de "Información del código fuente"
  	  
  	  limpiarNombreEmpresa: function(){

  	  	this.errores.input_nombre_empresa = "";

  	  },

  	  limpiarRepositorioRemoto: function(){

  	  	this.errores.input_repositorio_remoto = "";

  	  },

  	  recargarPagina: function(){

          location.reload();

  	  }



    },


    computed: {

    	encuesta_vacia(){

    	if(

		this.input_nombre_aplicacion == '' &&
      	this.input_descripcion_software == '' &&
	    this.input_version_software == '' &&
	    
	    this.input_lenguaje_programacion == '' &&
	    this.input_lenguaje_programacion_otro == '' &&
	    this.input_lenguaje_programacion_version == '' &&
	    
	    this.input_base_dato == '' &&
	    this.input_base_dato_otra == '' &&
	    this.input_base_dato_version == '' &&


	    this.input_version_mobile == '' &&
	    this.input_version_web == '' &&


	    this.input_pasos_instalacion == '' &&

    
	    this.input_tipo_licencia == '' &&
	    this.input_tipo_licencia_otra == '' &&

	    this.input_desarrollo_codigo_fuente_disponible == '' &&


	    this.input_posee_repositorio == false &&
	   	this.input_repositorio_remoto == '' &&


	    this.input_desarrollo_empresa == false &&
	   	this.input_nombre_empresa == '' &&

	    this.input_reparticion == '' &&

	    this.input_responsable_nombre == '' &&
	    this.input_responsable_correo == '' &&
	    this.input_responsable_telefono_celular == '' &&
	    this.input_responsable_telefono_interno == '' &&

	    this.user_email == '' &&
	    this.user_password == ''

    	)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}	


    	},

    	encuesta_completa(){

    		 if(this.save_form_reparticion &&this.save_form_responsable && this.save_form_software && this.save_form_codigo_fuente && this.save_form_instalacion && this.save_form_licencia )
    			return true;
    	},

    	otra_licencia(){

				if(this.input_tipo_licencia == 'Otra ...') 	return true;

    	},

    	otro_lenguaje(){

				if(this.input_lenguaje_programacion == 'Otro ...') 	return true;

    	},

    	otra_db(){

				if(this.input_base_dato == 'Otra ...') 	return true;

    	},

    	_errores_form_instalacion(){
    		 if(this.errores_form_instalacion) return true;
    		  else false;
    	},


    },

    mounted : function (){

    	this.getAplicaciones();
    	this.getInfoGeneral();

    }

  });

vm.$watch('input_lenguaje_programacion', function(){

	this.errores.lenguaje_nom = "";
});

vm.$watch('input_base_dato', function(){

	this.errores.base_nom = "";
})

vm.$watch('input_tipo_licencia', function(){

	this.errores.input_tipo_licencia = "";
})

vm.$watch('input_tipo_licencia_otra', function(){

	this.errores.input_tipo_licencia = "";
})


//limpiar errores en seccion INFORMACIÓN GENERAL

vm.$watch('informacion_general.aplicacion_ofimatica', function(){

	this.informacion_general.otra_aplicacion_ofimatica = (this.informacion_general.aplicacion_ofimatica == 'Otro ...') ? '-' : '';

	this.informacion_general.error_aplicacion_ofimatica = '';
	
	this.informacion_general.hay_errores = false; //limpiar el error de la sección

});

vm.$watch('informacion_general.sistema_operativo', function(){

	this.informacion_general.otro_sistema_operativo = (this.informacion_general.sistema_operativo == 'Otro ...') ? '-' : '';

	this.informacion_general.error_sistema_operativo = '';

	this.informacion_general.hay_errores = false; //limpiar el error de la sección

});




$('#progress_bar').progress({ total: 6 });


//inicializo el acordion

$('.ui.accordion').accordion();


$('#reparticion')
  .popup({
    inline: true,
    popup: '#popup_reparticion',
    position   : 'bottom left',

  })
;


</script>



@endsection
