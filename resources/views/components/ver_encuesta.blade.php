
@extends('layouts.app')

@section('estilos')
<style type="text/css">


</style>


@endsection

@section('content')

<img class="img-nav" src="assets/img/codigo.jpg"></img>

<div class="ui container" id="manage-vue">	

	<div class="ui menu">
	  
	 <div class="left menu">
			
			<button @click.prevent="volverAEncuesta">Volver a la encuesta</button>	
		
	 </div>

	  <div class="right menu">
	  
	     @if(Auth::guest())
	    
	      <div class="item">
	        {{-- <a @click="setModalOn" ><div class="ui primary button">Log-in</div></a> --}}
	       <a href="{{route('auth/login')}}"> <div class="ui primary button">Acceder</div></a>
	      </div>
	    
	    @else

		   <div class="item">
				Bienvenido/a {{ Auth::user()->name }}
		   </div>
	       <div class="item">
	          <a href="{{route('auth/logout')}}"><div class="ui primary button">Cerrar sesión</div></a>
	       </div>

	    @endif
	  
	  </div>
	 
	</div>

	<div class="ui form">

			<div class="ui grid">

			  <div class="one column row">
			

						{{-- PCS WINDOWS --}}


						 <div class="centered floated column" style="margin-top:2%;">

						 	<div class="field" v-bind:class="{'disabled' : informacion_general.save_form, 'error' : informacion_general.error_pcs_windows}">

								<label>¿Cuantas PCs posee el organismo con sistemas operativos Windows? </label>

								{{-- <select v-model="informacion_general.aplicacion_ofimatica" >
									<option v-for="ofimatica in all_ofimaticas" :value="ofimatica.nombre">
										
										@{{ofimatica.nombre}}
								
									</option>
								</select> --}}

								<input 
									   v-model="informacion_general.pcs_windows"
									   type="text" 
									   placeholder="." 
								/>

								<label class="error" v-show="informacion_general.error_pcs_windows"> @{{informacion_general.error_pcs_windows}} </label>
							
							</div>
						
						</div>			



						{{-- PCS LINUX --}}


						 <div class="centered floated column" style="margin-top:2%;">

						 	<div class="field" v-bind:class="{'disabled' : informacion_general.save_form, 'error' : informacion_general.error_pcs_windows}">

								<label>¿Cuantas PCs posee el organismo con sistemas operativos libres (Ubuntu, Debian, Linux Mint, etc)? </label>

								{{-- <select v-model="informacion_general.aplicacion_ofimatica" >
									<option v-for="ofimatica in all_ofimaticas" :value="ofimatica.nombre">
										
										@{{ofimatica.nombre}}
								
									</option>
								</select> --}}

								<input  
									   v-model="informacion_general.pcs_linux"
									   type="text" 
									   placeholder="." 
								/>

								<label class="error" v-show="informacion_general.error_pcs_linux"> @{{informacion_general.error_pcs_linux}} </label>
							
							</div>
						
						</div>	



						{{-- OFIMATICA LIBRE --}}


						 <div class="centered floated column" style="margin-top:2%;">

						 	<div class="field" v-bind:class="{'disabled' : informacion_general.save_form, 'error' : informacion_general.error_ofimatica_libre}">

								<label>¿Ingrese de la herramienta de Ofimática Libre (LibreOffice, OpenOffice) de preferencia? (Que no sea Microsoft Office) </label>

								{{-- <select v-model="informacion_general.aplicacion_ofimatica" >
									<option v-for="ofimatica in all_ofimaticas" :value="ofimatica.nombre">
										
										@{{ofimatica.nombre}}
								
									</option>
								</select> --}}

								<input  
									   v-model="informacion_general.ofimatica_libre"
									   type="text" 
									   placeholder="." 
								/>

								<label class="error" v-show="informacion_general.error_ofimatica_libre"> @{{informacion_general.error_ofimatica_libre}} </label>
							
							</div>
						
						</div>		




						{{-- ANTIVIRUS EN USO --}}


						<div class="centered floated column" style="margin-top:2%;">

						 	<div class="field" v-bind:class="{'disabled' : informacion_general.save_form,
						 	'error' : errors.has('input_informacion_general_antivirus')}">

								<label>Ingrese el antivirus de preferencia (de usar uno) : </label>

								<input  
									   name="input_informacion_general_antivirus" 
									   type="text" 
									   v-model="informacion_general.antivirus"
								/>

						
							</div>
						
						 </div>

						
						{{-- BOTON GUARDAR Y CANCELAR --}}

						<div class="holder_boton_guardar_cancelar" >
										
						<div class="centered floated column" style="margin-top:10%;">
										    		
									<template v-if="!informacion_general.habilitar_edicion">

										<div class="field" >
										 	<div class="ui buttons">
												
												<button class="ui button">Cancelar</button>
												<div class="or"></div>
												<button @click="onGuardarInformacionGeneral" class="ui blue button active">Guardar</button>
											
											</div>
										</div>

									</template>

									<template v-else>
												

										<div class="field">
										 	<div class="ui buttons" style="text-align: center;">
												
												<button class="ui button">Cancelar</button>
												<div class="or"></div>
											<button @click="onEditarInformacionGeneral" class="orange ui button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar los datos</button>
											
											</div>
										</div>

									</template>
										   
							
							</div>

						</div>

				</div>
				</div>
		</div>


</div>



@endsection


@section('scripts')

{{-- <script type="text/javascript" src="http://closure-compiler.appspot.com/code/jsc81247ddc5c50e240d900582629789a85/default.js" ></script>
 --}}

<script type="text/javascript">


  Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr('value');

  Vue.use(VeeValidate);
  Vue.component('v-select', VueSelect.VueSelect);


  var vm = new Vue({

    el: '#manage-vue',


    data: function(){

      return {


      	usuario_logeado_organismo_id : '{{ !Auth::guest() ?  \Auth::user()->organismo_id : ''}}' ,

      	
      	//variables de la sección de ENCUESTAS GENERALES

	    encuesta_info_general: '', //encuesta que traemos de haberla ya cargado

	    informacion_general : {
	    	
	    	hay_errores: false,

	    	habilitar_edicion: false,

		  
			pcs_windows: '',
			error_pcs_windows: '',	

			pcs_linux: '',
			error_pcs_linux: '',
	  
	  		ofimatica_libre:'',
	  		error_ofimatica_libre:'',

		    
		    antivirus: '',
		    error_antivirus: '',

	    },


	    	
	    //fin variables de seccion de ENCUESTAS GENERALES

        path: window.location.href.slice(0,-29)


      }

    },


    methods: {

      getInformacionGeneral : function(){

      	let organismo_id = this.usuario_logeado_organismo_id;

      	this.$http.post( this.path + '/informacion_general/get_by_organismo',{organismo_id}
      		).then((response) => {

      		this.$set(this, 'informacion_general', response.data[0]);

      		this.informacion_general.habilitar_edicion = true;

      		},(response) => {



      		});


      },


  	 onGuardarInformacionGeneral: function(){


  	  let pcs_windows = this.informacion_general.pcs_windows;
  	  let pcs_linux = this.informacion_general.pcs_linux;
  	  let ofimatica_libre = this.informacion_general.ofimatica_libre;		  	  
  	  let antivirus = this.informacion_general.antivirus;



  	  this.$http.post( this.path + '/informacion_general/store',
  	  				{

  	  				pcs_windows,
  	  				pcs_linux,
  	  				ofimatica_libre,
  	  				antivirus

  	  				}).then( (response) => {

  	  				toastr.success('','Operación exitosa',{timeOut: 5000});

  	 				}, (response) => {


  	 				});

  	  },

  	  onEditarInformacionGeneral : function(){

  	  let informacion_general = this.informacion_general;

  	  this.$http.post( this.path + '/informacion_general/edit',
  	  				{

  	  				informacion_general

  	  				}).then( (response) => {

  	  				toastr.success('La encuesta fue modificada correctamente.','¡Operación exitosa!', {timeOut: 5000});

  	 				}, (response) => {


  	 				});

  	  },

  	  volverAEncuesta : function()
  	  {
  	  	window.location = this.path;
  	  }

    
    },


    computed: {


    },

    mounted : function (){

    this.getInformacionGeneral();


    }

  });



</script>


@endsection