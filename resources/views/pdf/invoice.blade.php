<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Example 2</title>
    {!! Html::style('assets/css/pdfPAC.css') !!}
  </head>
  <body>
 
    <main>
      <div id="details" class="clearfix">
        <div id="invoice">
{{--           <h1>INVOICE {{ $invoice }}</h1>
 --}}
          
          <div class="date">Date of Invoice: {{ $date }}</div>
        </div>
      </div>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="no">#</th>
            <th class="desc">Nombre</th>
            <th class="unit">Email</th>
            <th class="total">Pass</th>
          </tr>
        </thead>
        <tbody>
        @foreach ( $datas as $data)

          <tr>
            <td class="no">{{ $data['id'] }}</td>
            <td class="desc">{{ $data['nombre'] }}</td>
            <td class="unit">{{ $data['email'] }}</td>
            <td class="total">{{ $data['pass'] }} </td>
          </tr>
        @endforeach
 
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <td >TOTAL</td>
            <td>$6,500.00</td>
          </tr>
        </tfoot>
      </table>
  </body>
</html>