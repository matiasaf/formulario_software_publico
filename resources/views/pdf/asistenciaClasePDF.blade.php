<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Asistencia</title>
    {!! Html::style('assets/css/pdfPAC.css') !!}
  </head>
  <body>
  <img src= {!! URL::to('assets/img/PAC_encabezado.png') !!} ?> />
    <main>
      <!-- <div id="imagen" style="width: 100%; height: auto;"> </div> -->
      <div id="details" class="clearfix">
        <div id="invoice">
          <h1> Curso: {{ $curso }}</h1>
          <h2> Materia: {{ $materia }}</h2>

          <div class="date">Fecha de la Clase: {{ $date }}</div>

        </div>
      </div>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="no">#</th>
            <th class="desc">Nombre</th>
            <th class="unit">Email</th>
            <th class="total">Pass</th>
          </tr>
        </thead> 
        <tbody>
        @foreach ( $datas as $data)

          <tr>
            <td class="no">{{ $data['id'] }}</td>
            <td class="desc">{{ $data['nombre'] }}</td>
            <td class="unit">{{ $data['email'] }}</td>
            <td class="total">{{ $data['pass'] }} </td>
          </tr>
        @endforeach
 
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <td >TOTAL</td>
            <td>$6,500.00</td>
          </tr>
        </tfoot>
      </table>
  </body>
</html>