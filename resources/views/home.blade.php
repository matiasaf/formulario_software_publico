

@extends('layouts.app')


@section('content')


@endsection

@section('scripts')

<script type="text/javascript">

    Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr('value');

    var vm = new Vue({


        el: '#manage-vue',


        data: function(){

            return {

                usuarios: [],

                path : location.origin+'/notariado/rpi/capacitacion-laravel-public',

            }

        },


        methods: {

           getUsuarios: function(){


            this.$http.get( this.path +'/usuarios').then((response) =>{


              this.$set('usuarios', response.data);


          }, (response) =>{


            });
        },

 

        
},
			ready: function() {

				this.getUsuarios();

			}

});
</script>


@endsection