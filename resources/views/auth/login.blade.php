
@extends('layouts.app')
<!-- resources/views/auth/login.blade.php -->

@section('estilos')
  
<style type="text/css">
    

  body {
    /*background: url('https://ununsplash.imgix.net/uploads/141362941583982a7e0fc/abcfbca1?q=75&fm=jpg&s=5266baf09e0e878b72b2e34adf2f54a0') fixed;*/
     background: -webkit-linear-gradient(#ffffff, #000000);
    background: -moz-linear-gradient(#ffffff, #000000);
    background: -o-linear-gradient(#ffffff, #000000);
    background-size: cover;
    padding: 0;
    margin: 0;
  }

  .form-holder {
    background: rgba(255,255,255,0.2);
    margin-top: 10%;
    border-radius: 3px;
  }

  .form-head {
    font-size: 30px;
    letter-spacing: 2px;
    text-transform: uppercase;
    color: #fff;
    text-shadow: 0 0 30px #000;
    margin: 15px auto 30px auto;
  }

  .remember-me {
    text-align: left;
  }
  .ui.checkbox label {
    color: #ddd;
  }

</style>


@endsection




@section('content')



<div class="ui container" id="manage-vue">  


@if(count($errors) > 0)
  
  <br> 
  <div class="ui negative message">
    <div class="header">
      ¡Hay errores al loguearse!
    </div>
    <ul>
     @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
     @endforeach
    </ul>
  </div>

@endif  


    <div class="ui one column center aligned grid">
      <div class="column six wide form-holder">
        <h2 class="center aligned header form-head">Iniciar Sesión</h2>
        <div class="ui form">
        <form method="post" action="{{route('auth/login')}}">
          
          {{ csrf_field() }}

          <div class="field">
            <input type="text" name="email" placeholder="E-mail:">
          </div>
          <div class="field">
            <input type="password" name="password" placeholder="Password:">
          </div> 
          <div class="field">
            <input type="submit" value="Acceder" class="ui button large fluid green">
          </div>
          <div class="inline field">
            <div class="ui checkbox">
              <input type="checkbox" name="remember">
              <label>Recuerdame</label>
            </div>
          </div>

          </form>

        </div>
      </div>
    </div>


</div>



    
@endsection
