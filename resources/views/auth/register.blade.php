

@extends('layouts.app')

@section('estilos')
  
<style type="text/css">
    

  body {
    background: url('https://ununsplash.imgix.net/uploads/141362941583982a7e0fc/abcfbca1?q=75&fm=jpg&s=5266baf09e0e878b72b2e34adf2f54a0') fixed;
    background-size: cover;
    padding: 0;
    margin: 0;
  }

  .form-holder {
    background: rgba(255,255,255,0.2);
    margin-top: 10%;
    border-radius: 3px;
  }

  .form-head {
    font-size: 30px;
    letter-spacing: 2px;
    text-transform: uppercase;
    color: #fff;
    text-shadow: 0 0 30px #000;
    margin: 15px auto 30px auto;
  }

  .remember-me {
    text-align: left;
  }
  .ui.checkbox label {
    color: #ddd;
  }

  .font-error{

    color: #9f3a38;

 }

</style>


@endsection




@section('content')



<div class="ui container" id="manage-vue">  

    <div class="ui one column center aligned grid">
      <div class="column six wide form-holder">
        <h2 class="center aligned header form-head">Registro al formulario de software público</h2>
        <div class="ui form">
         
          <div class="field" v-bind:class="{'error' : errors.has('name') || errores.name}">
            <input @keyup="limpiarNombre" v-validate="'required'" type="text" name="name" id="name" placeholder="Nombre:" v-model="name">
            
            <label class="font-error" v-show="errors.has('name')">@{{errors.first('name')}}</label>
            <label class="font-error" v-if="errores.name">@{{errores.name[0]}}</label>
          
          </div>

          <div class="field" v-bind:class="{'error' : errors.has('name') || errores.email }">      
            <input @keyup="limpiarEmail" v-validate="'required'" type="text" name="email" id="email" placeholder="E-mail:" v-model="email">

            <label class="font-error" v-show="errors.has('email')">@{{errors.first('email')}}</label>
            <label class="font-error" v-if="errores.email">@{{errores.email[0]}}</label>
          </div>

          <div class="field" v-bind:class="{'error' : errors.has('name') || errores.password}">
            <input @keyup="limpiarPassword" type="password" name="password" id="password" placeholder="Password:" v-model="password"/>
            
            <label class="font-error" v-show="errors.has('password')">@{{errors.first('password')}}</label>
            <label class="font-error" v-if="errores.password">@{{errores.password[0]}}</label>
          </div>

          <div class="field" v-bind:class="{'error' : errores.password}">
            <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Confirmación de password:" v-model="password_confirmation" />
        
         </div>

          <div class="field" v-bind:class="{ 'error' : errores.organismo }">
              
              <select v-validate="'required'" name="organismo" id="organismo" v-model="organismo">
                <option value="organismo_1">Organismo 1</option>
                <option value="organismo_2">Organismo 2</option>
                <option value="organismo_3">Organismo 3</option>
              </select>

            <label class="font-error" v-show="errors.has('organismo')">@{{errors.first('organismo')}}</label>
            <label class="font-error" v-if="errores.organismo">@{{errores.organismo[0]}}</label>

          </div>

          <div class="field">
            <button @click="registrarUsuario" class="ui button large fluid green">Registrar</button>
          </div>


     
       {{--    <div class="inline field">
            <div class="ui checkbox">
              <input type="checkbox">
              <label>Recuerdame</label>
            </div>
          </div> --}}
        </div>
      </div>
    </div>

</div>
    
@endsection

@section('scripts')

{{-- <script type="text/javascript" src="http://closure-compiler.appspot.com/code/jsc81247ddc5c50e240d900582629789a85/default.js" ></script>
 --}}

<script type="text/javascript">


  Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr('value');


  Vue.use(VeeValidate);
  Vue.component('v-select', VueSelect.VueSelect);


  var vm = new Vue({

    el: '#manage-vue',


    data: function(){

      return {

        name: '',
        email: '',
        password: '',
        password_confirmation: '',
        organismo: '',

        errores:[],


        path: window.location.href.slice(0,-20)
       
        //path: window.location.href


      }

    },


    methods: {

      registrarUsuario: function(){


        this.$http.post( this.path +'/auth/register',
                                {
                                    name : this.name,
                                    email: this.email,
                                    password : this.password,
                                    password_confirmation: this.password_confirmation,
                                    organismo : this.organismo

                                } ).then((response) =>{

                                window.location = this.path + '/test';

                                }, (response) => {

                                    toastr.error( 'Revise el formulario.', '¡Hay problemas!', {timeOut: 5000});

                                    this.$set(this, 'errores', response.data);


                                });

    

      },

      limpiarNombre : function(){

        this.errores.name = "";

      },

      limpiarEmail : function(){

        this.errores.email = "";

      },

      limpiarPassword : function(){

        this.errores.email = "";

      },


    },



    mounted : function (){



    }

  });

  vm.$watch('organismo' , function(){

    this.errores.organismo = "";

  });

  </script>


@endsection

