<!DOCTYPE html>

<html>
<head>

	<meta http-equiv="Content-type" content="text/html;charset=utf-8"  />
	
	<title>	@yield('title') </title>
	

	<meta id="token" name="token" value="{{ csrf_token() }}">

	<link href="https://fonts.googleapis.com/css?family=Cabin+Sketch" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		
	<link rel="stylesheet" href="{{URL::to('assets/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{URL::to('assets/css/semantic.min.css')}}">


  <!-- Material Design fonts -->
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">


	@yield('estilos')


</head>

<body>


		@yield('content')


		<!-- cargo libreria moment.js -->

    <script src="{{URL::to('assets/js/moment.js')}}"></script>
      
		<script src="https://unpkg.com/vue/dist/vue.js"></script>


    <script src="{{URL::to('assets/js/vee-validate.min.js')}}"></script>
    <script src="{{URL::to('assets/js/vue-resource.min.js')}}"></script>
        
    <script
      src="https://code.jquery.com/jquery-3.1.1.min.js"
      integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
      crossorigin="anonymous">
  
    </script>

    {{-- Semantic UI --}}
    <script src="{{URL::to('assets/js/semantic.min.js')}}"></script>
        
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <script src="{{URL::to('assets/js/underscore-min.js')}}"></script>

		  
		<!-- use the latest release -->
		<script src="https://unpkg.com/vue-select@latest"></script>
    
		@yield('scripts')

		<script>


  function IsNumeric(valor) 
  { 
      var log=valor.length; var sw="S"; 
      for (x=0; x<log; x++) 
        { v1=valor.substr(x,1); 
          v2 = parseInt(v1); 
          //Compruebo si es un valor numérico 
          if (isNaN(v2)) { sw= "N";} 
        } 
        if (sw=="S") {return true;} else {return false; } 
  }

  function formateafecha(fecha) 
  { 
        var long = fecha.length; 
        var dia; 
        var mes; 
        var ano; 

        var primerslap=false; 
        var segundoslap=false; 
      
        if ((long>=2) && (primerslap==false)) { dia=fecha.substr(0,2); 
          if ((this.IsNumeric(dia)==true) && (dia<=31) && (dia!="00")) { fecha=fecha.substr(0,2)+"/"+fecha.substr(3,7); primerslap=true; } 
          else { fecha=""; primerslap=false;} 
        } 
        else 
          { dia=fecha.substr(0,1); 
            if (this.IsNumeric(dia)==false) 
              {fecha="";} 
            if ((long<=2) && (primerslap=true)) {fecha=fecha.substr(0,1); primerslap=false; } 
          } 
          if ((long>=5) && (segundoslap==false)) 
            { mes=fecha.substr(3,2); 
              if ((this.IsNumeric(mes)==true) &&(mes<=12) && (mes!="00")) { fecha=fecha.substr(0,5)+"/"+fecha.substr(6,4); segundoslap=true; } 
              else { fecha=fecha.substr(0,3);; segundoslap=false;} 
            } 
            else { if ((long<=5) && (segundoslap=true)) { fecha=fecha.substr(0,4); segundoslap=false; } } 
            if (long>=7) 
              { ano=fecha.substr(6,4); 
                if (this.IsNumeric(ano)==false) { fecha=fecha.substr(0,6); } 
                else { if (long==10){ if ((ano==0) || (ano<1900) || (ano>2100)) { fecha=fecha.substr(0,6); } } } 
              }

              if (long>=10) 
              { 
                fecha=fecha.substr(0,10); 
                dia=fecha.substr(0,2);  // ubicacion fin de dia
                mes=fecha.substr(3,2); // ubicacion fin de mes
                ano=fecha.substr(6,4);  // ubicacion fin de año
                //
                // Año no viciesto y es febrero y el dia es mayor a 28

              if ( (ano%4 != 0) && (mes == 02) && (dia > 28) ) { fecha=fecha.substr(0,2)+"/"; }
              if((ano > 2100)){fecha = fecha.substr(3,2)+"/"}
              if((mes == 04) && dia >30){fecha = fecha.substr(0,2)+"/"}
              if((mes == 06) && dia >30){fecha = fecha.substr(0,2)+"/"}
              if((mes == 09) && dia >30){fecha = fecha.substr(0,2)+"/"}
              if((mes == 11) && dia >30){fecha = fecha.substr(0,2)+"/"}
            } 

          return (fecha); 
    }

  </script>

</body>

</html>