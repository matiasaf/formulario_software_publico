<nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href={{route('view.home')}}><img src={{route('img/logo-er.png'}}></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    
                    @if(Auth::guest())
                    
                    <li><a href={{route('view.formulario.registro-inscriptos')}}>Inscripción</a></li>
                    
                    @else
                    
                    <li><a href={{route('view.formulario.registro-inscriptos')}}>Inscripción</a></li>
                    <li><a href={{route('view.tabla.inscriptos')}}>Ver inscriptos</a></li>
                    <li><a href={{route('cursos.index')}}>Cursos</a></li>
                    <li><a href={{route('matricula.index')}}>Matricular</a></li>
                    <li><a href={{route('clases.index')}}>Clases</a></li>
                    <li><a href={{route('asistencias.index')}}>Asistencias</a></li>
                    <li><a href={{route('calificaciones.index')}}>Calificaciones</a></li>

                    @endif
                    
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    
                    @if (Auth::guest())
                        
                        <li><a href="{{route('auth/login')}}">Login</a></li>
                        <li><a href="{{route('auth/register')}}">Registrarse</a></li>
                    
                    @else
                      
                        <li>
                            <a href="#">{{ Auth::user()->name }}</a>
                        </li>
                      
                        <li><a href="{{route('auth/logout')}}">Logout</a></li>
                        
                    @endif
                </ul>
            </div>
        </div>
    </nav>