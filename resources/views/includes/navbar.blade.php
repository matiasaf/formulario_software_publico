


<div class="ui menu">

  <div class="right menu">
  
    @if(Auth::guest())
     
      <div class="item">
        <a href="{{route('auth/login')}}"><div class="ui primary button">Log-in</div></a>
      </div>
    
    @else
       <div class="item">
        <a href="#"><div class="ui primary button">{{ Auth::user()->name }}</div></a>
      </div>
    @endif

  </div>
 
</div>


{{-- 
<div class="ui small menu">
  <a class="active item">
    Recopilación de Aplicaciones de la Administración Pública
  </a>

  <div class="right menu">
    
    <div class="item">
        <div class="ui primary button">Sign Up</div>
    </div>
  </div>
</div> --}}

{{-- 
<div class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="javascript:void(0)">Recopilación de Aplicaciones de la Administración Pública</a>
    </div>
    <div class="navbar-collapse collapse navbar-responsive-collapse">
      <ul class="nav navbar-nav">
       
      </ul>
   
      <ul class="nav navbar-nav navbar-right">
        <li><a href="javascript:void(0)">Link</a></li>
        <li class="dropdown">
        
          <ul class="dropdown-menu">
            <li><a href="javascript:void(0)">Action</a></li>
            <li><a href="javascript:void(0)">Another action</a></li>
            <li><a href="javascript:void(0)">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="javascript:void(0)">Separated link</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</div> --}}