<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Aplicacion;
use App\Responsable;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;


class AplicacionController extends Controller
{

   
	public function store(Request $request)
	{

		  $rules=[ 
                  'nombre_aplicacion' => 'required',
                  'version_sof' => 'required',
                  'descripcion' => 'required',
                  'lenguaje_nom' => 'required',
                  'lenguaje_ver' => 'required',
                  'version_mob' => 'required',
                  'version_web' => 'required',
                  'base_nom' => 'required',
                  'base_ver' => 'required',

                  'instalacion' => 'required',

                  'desa_por_emp' => 'required',
                  'empresa_nom'  => 'required',
                  'cod_fuente'  => 'required',
                  'repositorio' => 'required',

                  'reparticion' => 'required',

                  'licencia' => 'required',

          ];

          $msg = [

                  'nombre_aplicacion.required' => 'Debe indicar el respectivo nombre de la aplicación',
                  'version_sof.required' => 'Debe indicar una version del software.',
                  'descripcion.required' => 'Debe indicar una descripción.',
                  'lenguaje_nom.required' => 'Debe indicar nombre del lenguaje.',
                  'lenguaje_ver.required' => 'Debe indicar versión del lenguaje .',
                  'version_mob.required' => 'Debe seleccionar si posee version mobile.',
                  'version_web.required' => 'Debe seleccionar si posee version web.',
                  'base_nom.required' => 'Debe insertar el nombre de la base de dato utilizada.',
                  'base_ver.required' => 'Debe indicar la version de la base de datos.',

                  'instalacion.required' => 'Debe completar con los pasos para su correcta instalación.',
                  
                  'desa_por_emp' => 'Fue desarrollado por una empresa',
                  'empresa_nom.required'  => 'Ingrese la empresa.',
                  'cod_fuente.required'  => 'Posee codigo fuente',
                  'repositorio.required' => 'Ingrese el repositorio',
                  
                  'reparticion.required' => 'Debe completar con las reparticiones en la que se encuentra instalado.',
                  
                  'licencia.required' => 'Debe seleccionar una licencia.'

          ];


          // $this->validate($request,$rules,$msg);
	
          $id_aplicacion = Aplicacion::insertGetId( array(
                        
                        'nombre_aplicacion' => $request->nombre_aplicacion,
                        'version_sof'  =>  $request->version_sof,
                        'descripcion'  =>  $request->descripcion,
                        'lenguaje_nom' =>  $request->lenguaje_nom,
                        'lenguaje_ver' =>  $request->lenguaje_ver,
                        'version_mob'  =>  $request->version_mob,
                        'version_web'  =>  $request->version_web,
                        'base_nom' =>  $request->base_nom,
                        'base_ver' =>  $request->base_ver,

                        'instalacion' =>  $request->instalacion,

                        'desa_por_emp' => $request->desa_por_emp,
                        'empresa_nom'  => $request->empresa_nom,
                        'cod_fuente'   => $request->cod_fuente,
                        'repositorio'  => $request->repositorio,
                              
                        'reparticion' => $request->reparticion,

                        'licencia' => $request->licencia,

                        'usuario' => \Auth::user()->id

                        )
             );



            foreach ($request->responsables as $responsable) {


                  Responsable::insertGetId(array(
                      
                      'responsable' => $responsable['nombre'],

                      'correo' => $responsable['correo'],
          
                      'celular' => $responsable['telefono_celular'],

                      'telefono' => $responsable['telefono_interno'],

                      'app_id' => $id_aplicacion
                               
                        )
                  );

            }


            return response()->json([ 'id_aplicacion' => $id_aplicacion ]);


	}


  public function edit(Request $request)
  {
      $rules=[ 
                  'nombre_aplicacion' => 'required',
                  'version_sof' => 'required',
                  'descripcion' => 'required',
                  'lenguaje_nom' => 'required',
                  'lenguaje_ver' => 'required',
                  'version_mob' => 'required',
                  'version_web' => 'required',
                  'base_nom' => 'required',
                  'base_ver' => 'required',

                  'instalacion' => 'required',

                  'desa_por_emp' => 'required',
                  'empresa_nom'  => 'required',
                  'cod_fuente'  => 'required',
                  'repositorio' => 'required',

                  'reparticion' => 'required',

                  'licencia' => 'required',

          ];

          $msg = [

                  'nombre_aplicacion.required' => 'Debe indicar el respectivo nombre de la aplicación',
                  'version_sof.required' => 'Debe indicar una version del software.',
                  'descripcion.required' => 'Debe indicar una descripción.',
                  'lenguaje_nom.required' => 'Debe indicar nombre del lenguaje.',
                  'lenguaje_ver.required' => 'Debe indicar versión del lenguaje .',
                  'version_mob.required' => 'Debe seleccionar si posee version mobile.',
                  'version_web.required' => 'Debe seleccionar si posee version web.',
                  'base_nom.required' => 'Debe insertar el nombre de la base de dato utilizada.',
                  'base_ver.required' => 'Debe indicar la version de la base de datos.',

                  'instalacion.required' => 'Debe completar con los pasos para su correcta instalación.',
                  
                  'desa_por_emp' => 'Fue desarrollado por una empresa',
                  'empresa_nom.required'  => 'Ingrese la empresa.',
                  'cod_fuente.required'  => 'Posee codigo fuente',
                  'repositorio.required' => 'Ingrese el repositorio',
                  
                  'reparticion.required' => 'Debe completar con las reparticiones en la que se encuentra instalado.',
                  
                  'licencia.required' => 'Debe seleccionar una licencia.'

          ];


          $this->validate($request,$rules,$msg);


          $aplicacion = Aplicacion::find($request->id);

          $aplicacion->nombre_aplicacion = $request->nombre_aplicacion;
          $aplicacion->version_sof = $request->version_sof;
          $aplicacion->descripcion = $request->descripcion;
          $aplicacion->lenguaje_nom = $request->lenguaje_nom;
          $aplicacion->lenguaje_ver = $request->lenguaje_ver;
          $aplicacion->version_mob = $request->version_mob;
          $aplicacion->version_web = $request->version_web;
          $aplicacion->base_nom = $request->base_nom;
          $aplicacion->base_ver = $request->base_ver;
         
          $aplicacion->instalacion = $request->instalacion;
         
          $aplicacion->desa_por_emp = $request->desa_por_emp;
          $aplicacion->empresa_nom = $request->empresa_nom;

          $aplicacion->cod_fuente = $request->cod_fuente;
          $aplicacion->repositorio = $request->repositorio;
          $aplicacion->reparticion = $request->reparticion;
          $aplicacion->licencia = $request->licencia;

          $aplicacion->save();

          return response()->json(['message' => 'Los cambios fueron aplicados correctamente.']);


  }

  public function delete(Request $request)
  {

    $aplicacion = Aplicacion::find($request->id);

    $aplicacion->delete();

    $responsables = Responsable::where('app_id' , $request->id)->get();


    foreach ($responsables as $responsable) {

        $responsable->delete();

    }

    return response()->json(['message' => 'La aplicación fue eliminada con éxito']);

  }

  public function verAplicacionesCargadasPorUsuario()
  {

   $aplicaciones = Aplicacion::with('responsables')
                              ->where( 'usuario_id', \Auth::user()->id )
                              ->get(); 

   return response()->json($aplicaciones);

  }

  public function verVistaAplicaciones()
  {

    return view('components.ver_aplicaciones');
  
  }

}
