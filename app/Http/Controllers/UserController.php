<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Usuario;
use App\Post;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;


class UserController extends Controller
{

   
    public function get(){

    	$usuarios = Usuario::all();

    	return response()->json($usuarios);
    }

    public function createPost(Request $request)
    {
    	
  			Post::insertGetId(array(

            'titulo' => $request->titulo,
            'texto' => $request->texto
            
            ));

        return response()->json('Todo ok!');
    }


}
