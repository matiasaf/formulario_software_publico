<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\InformacionGeneral;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;


class InformacionGeneralController extends Controller
{

   
  public function getByOrganismo(Request $request)
  {

    $informacion_general = InformacionGeneral::where('organismo_id' , $request->organismo_id)
                        ->get();


    if($informacion_general->isEmpty()) return response()->json(['error' => 'no se encontro la encuesta' ],404);


    return response()->json($informacion_general);


  }

  public function getViewEncuesta()
  {

    return view('components.ver_encuesta');
    

  }

	public function store(Request $request)
	{

		      $rules=[ 


          ];

          $msg = [


                    
          ];


          // $this->validate($request,$rules,$msg);
	
          $info_general = InformacionGeneral::insertGetId( array(
                        
                        'pcs_windows' => $request->pcs_windows,
               
                        'pcs_linux' => $request->pcs_linux,

                        'ofimatica_libre' => $request->ofimatica_libre,
                        
                        'antivirus' => $request->antivirus,
    
                        'organismo_id' => Auth::user()->organismo_id

                        )
             );


          return response()->json('todo ok!');

    

	}


  public function edit(Request $request)
  {
          $rules=[ 

      
          ];

          $msg = [

  
          ];


          $this->validate($request,$rules,$msg);


          $informacion_general = InformacionGeneral::find($request->informacion_general['id']);


          $informacion_general->pcs_windows = $request->informacion_general['pcs_windows'];

          $informacion_general->pcs_linux = $request->informacion_general['pcs_linux'];

          $informacion_general->ofimatica_libre = $request->informacion_general['ofimatica_libre'];

          $informacion_general->antivirus = $request->informacion_general['antivirus'];

          

          $informacion_general->save();

          return response()->json(['message' => 'Los cambios fueron aplicados correctamente.']);


  }

  public function delete(Request $request)
  {

    $aplicacion = Aplicacion::find($request->id);

    $aplicacion->delete();

    $responsables = Responsable::where('app_id' , $request->id)->get();


    foreach ($responsables as $responsable) {

        $responsable->delete();

    }

    return response()->json(['message' => 'La aplicación fue eliminada con éxito']);

  }


}
