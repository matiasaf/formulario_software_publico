<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Responsable;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Storage;


class ResponsableController extends Controller
{

   
	public function store(Request $request)
	{

		//dd($request->responsables);

	}

	public function delete(Request $request)
	{

		$responsable = Responsable::find($request->id);

		$responsable->delete();

		return response()->json('todo ok!');

	}

	public function add(Request $request)
	{

		$rules = [

		'responsable' => 'required',
		'correo' => 'required | email'
		
		];

		$msj = [

		'responsable.required' => 'Debe ingresar un nombre del responsable.',
		'correo.required' => 'Debe ingresar un correo del responsable.',
		'correo.email' => 'Debe ingresar el mail con su formato correcto (prueba@prueba.net) .'

		];

        $this->validate($request,$rules,$msj);

		$id_responsable = Responsable::insertGetId(array(

				'responsable' => $request->responsable,
				'correo' => $request->correo,
				'telefono' => $request->telefono,
				'celular' => $request->celular,
				'app_id' => $request->app_id

			));


		return response()->json(['id' => $id_responsable]);

	}

	public function edit(Request $request)
	{

		$rules = [
		'responsable' => 'required',
		'correo' => 'required | email'
		];

		$msj = [

		'responsable.required' => 'Debe ingresar un nombre del responsable.',
		'correo.required' => 'Debe ingresar un correo del responsable.',
		'correo.email' => 'Debe ingresar el mail con su formato correcto (prueba@prueba.net) .'

		];

        $this->validate($request,$rules,$msj);

		$responsable =Responsable::find($request->id);

		$responsable->responsable = $request->responsable;
		$responsable->correo = $request->correo;
		$responsable->telefono = $request->telefono;
		$responsable->celular  = $request->celular;

		$responsable->save();

		return response()->json('Todo ok!');

	}

}
