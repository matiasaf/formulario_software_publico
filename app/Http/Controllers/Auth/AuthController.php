<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;


use Illuminate\Support\Facades\DB;


use Illuminate\Http\Request;

use App\Http\Requests;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    protected $redirectTo = '/';

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
 

    protected function crearUsuario(Request $request)
    {

       // dd($request);

         $rules =[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:FORM_USUARIOS',
            'password' => 'required|confirmed|min:6',
            'organismo' => 'required'
        ];

        $messages=[

            'name.required' => 'Ingrese el nombre del usuario a registrar.',
            
            'email.required' => 'Ingrese el e-mail del usuario a registrar.',
            'email.unique' => 'El e-mail ya se encuentra registrado, por favor utilice otro.',
            'email.email' => 'Debe poseer un formato email válido ( ejemplo@ejemplo.com ).',

            'password.required' => 'Ingrese el password del usuario a registar.',
            'password.min' => 'Debe tener un mínimo de 6 caracteres.',   
            'password.confirmed' => 'Las contraseñas deben coincidir.',

            'organismo.required' => 'Ingrese el organismo.' 

        ];

        $this->validate($request,$rules,$messages);

      
        User::insertGetId( array(
                        'name' => $request['name'],
                        'email' => $request['email'],
                        'PASSWORD' => bcrypt($request['password']),
                        'organismo' => $request['organismo']
                        )
        );

       return redirect()->route('index');
    
    }



}
