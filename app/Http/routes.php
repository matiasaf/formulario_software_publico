<?php


// require(app_path() . '/routes/clases.php');


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Rutas para la Autenticacion

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);
 

// Registration routes...

Route::get('__34__auth/register',[
	
	'uses' => 'Auth\AuthController@getRegister',
	'as' =>'auth.register'
	
	]);


Route::post('auth/register', ['as' => 'auth/register', 'uses' => 'Auth\AuthController@crearUsuario']);


Route::get('/', [

	'uses' => 'FormularioController@index', 
	'as' => 'index' 

	]	
);

Route::get('/test', [

	'uses' => 'FormularioController@index_test', 
	'as' => 'test' 

	]	
);

Route::get('user/get', [

	'middleware' => 'cors',
	'uses' => 'UserController@get'

	]);

Route::post('aplicacion/store', [

	'uses' => 'AplicacionController@store',
	'as'   => 'aplicacion.store'

	]);

Route::post('aplicacion/edit', [

	'uses' => 'AplicacionController@edit',
	'as'   => 'aplicacion.edit'

	]);

Route::post('aplicacion/delete', [

	'uses' => 'AplicacionController@delete',
	'as'   => 'aplicacion.delete'

	]);


Route::post('responsables/store', [

	'uses' => 'ResponsableController@store',
	'as'   => 'responsables.store'

	]);

Route::post('responsables/delete', [

	'uses' => 'ResponsableController@delete',
	'as'   => 'responsables.delete'

	]);

Route::post('responsables/add', [

	'uses' => 'ResponsableController@add',
	'as'   => 'responsables.add'

	]);

Route::post('responsables/edit', [

	'uses' => 'ResponsableController@edit',
	'as'   => 'responsables.edit'

	]);

Route::get('ver_aplicaciones', [

	'uses' => 'AplicacionController@verVistaAplicaciones',
	'as'   => 'aplicaciones.ver_vista_aplicaciones'

	]);


Route::get('aplicaciones/usuario', [

	'uses' => 'AplicacionController@verAplicacionesCargadasPorUsuario',
	'as'   => 'aplicaciones.usuario'

	]);





//RUTAS DE INFORMACION GENERAL


Route::post('informacion_general/store',[

	'uses' => 'InformacionGeneralController@store'

	]);

Route::post('informacion_general/edit',[

	'uses' => 'InformacionGeneralController@edit'

	]);

Route::post('informacion_general/get_by_organismo',[

	'uses' => 'InformacionGeneralController@getByOrganismo',
	'as'   => 'informacion_general.get_by_organismo'
	
	]);

Route::get('informacion_general/get_view',[

	'uses' => 'InformacionGeneralController@getViewEncuesta',
	'as'   => 'informacion_general.get_view'
	
	]);


Route::get('informacion_general/get_view_editar_encuesta',[

	'uses' => 'InformacionGeneralController@getViewEncuesta',
	'as'   => 'informacion_general.get_view_editar_encuesta'
	
	]);

