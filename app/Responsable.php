<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;
use Illuminate\Database\Eloquent\Model as Model;

class Responsable extends Model
{

     protected $table = 'FORM_RESPONSABLE';

     public $timestamps = false;

     public function aplicacion()
     {

     	return $this->belongsTo('App\Aplicacion', 'app_id');

     }


     
}
