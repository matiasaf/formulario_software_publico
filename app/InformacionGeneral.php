<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;
use Illuminate\Database\Eloquent\Model as Model;

class InformacionGeneral extends Model
{

     protected $table = 'FORM_INFO_GENERAL';

     public $timestamps = false;

}
