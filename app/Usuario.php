<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;
use Illuminate\Database\Eloquent\Model as Model;

class Usuario extends Model
{

     protected $table = 'FORM_USUARIOS';
     protected $primaryKey= 'ID';

}
