<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

use App\ValorFiscal;

use Illuminate\Support\Facades\DB;



class Helper
{

    public static function incrementId($id)
    {
        return ($id + 1);
    }


    public static function castear_descripcion($descripcion)
    {

        $descripcion_valor_fiscal=$descripcion->des;

        if($descripcion->des =="hasta ..." )
            $descripcion_valor_fiscal= substr($descripcion->des,0,5).' $'. self::dar_formato_numero($descripcion->imp_max);
        if($descripcion->des =="Entre ..." )
            $descripcion_valor_fiscal= substr($descripcion->des,0,5).' $'. self::dar_formato_numero($descripcion->imp_min).' y $'.self::dar_formato_numero($descripcion->imp_max);
        if($descripcion->des =="Mayor ..." )
            $descripcion_valor_fiscal= substr($descripcion->des,0,5).' a $'. self::dar_formato_numero($descripcion->imp_min);


        return $descripcion_valor_fiscal;
    }

    public static function dar_formato_numero($numero)
    {
    	
    	$resultado='';

        if (strlen($numero) == 6) 
        {
            $resultado = substr($numero,0,3) . '.' . substr($numero,3,3);
        } 
        else if( strlen($numero) == 7)
        {
            $resultado = substr($numero,0,1) . '.' . substr($numero,1,3) . '.' . substr($numero,4,3);
        } 


        return $resultado;

    }

    public static function obtenerNroDecreto()
    {
        
        $valor_fiscal = ValorFiscal::where('vig','si')->first();

        return $valor_fiscal->nro_decreto;

    }


    public static function verificarExistencia($dni)
    {

        $usuarios= DB::connection('oracle')
                    ->table('BDU.BDU_PERSONAS')
                    ->where('nro_documento',$dni)
                    ->get();

       dd($usuarios);

    }



}