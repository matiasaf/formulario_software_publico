<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;
use Illuminate\Database\Eloquent\Model as Model;

class Aplicacion extends Model
{

     protected $table = 'FORM_APLICACION';

     public $timestamps = false;


     public function responsables()
     {

     	return $this->hasMany('App\Responsable', 'app_id');

     }
     
}
